<?php

/**
 * Environment Declaration
 *
 * This switch statement sets our environment. The environment is used primarily
 * in our custom config file setup. It is also used, however, in the front-end
 * index.php file and the back-end admin.php file to set the debug mode
 *
 * @package    Focus Lab Master Config
 * @version    1.1.1
 * @author     Focus Lab, LLC <dev@focuslabllc.com>
 */

if ( ! defined('ENV'))
{
	switch (strtolower($_SERVER['HTTP_HOST'])) {
		case 'royhoustonphotography.co.uk' :
			define('ENV', 'prod');
			define('ENV_FULL', 'Production');
			define('ENV_SHOW_TAPE', FALSE);
			define('ENV_SYSTEM_ON', FALSE);
			define('ENV_DEBUG', FALSE);
		break;

		case 'royhouston.etheya-digitalcreativity.com' :
			define('ENV', 'dev');
			define('ENV_FULL', 'Development');
			define('ENV_SHOW_TAPE', TRUE);
			define('ENV_SYSTEM_ON', TRUE);
			define('ENV_DEBUG', FALSE);

		break;

		default :
			define('ENV', 'local');
			define('ENV_FULL', 'Local');
			define('ENV_SHOW_TAPE', TRUE);
			define('ENV_SYSTEM_ON', TRUE);
			define('ENV_DEBUG', TRUE);
		break;
	}
}

/* End of file config.env.php */
/* Location: ./config/config.env.php */