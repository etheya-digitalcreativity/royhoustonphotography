
$(document).ready(function(){
  $('#postage').popover(
  {
     trigger: 'hover',
     html: true,
     placement: 'left',
     content: 'Prices shown are in Pound’s Sterling (£) and include postage to addresses within the UK. If you require postage overseas please contact us by email and we will reply with an accurate shipping quote..'
  });
});



$(document).ready(function(){
  $('#sizes').popover(
  {
     trigger: 'hover',
     html: true,
     placement: 'top',
     content: 'For sizes or format options not listed, just email us with your requirements and we will provide a specific price.'
  });
});

$(document).ready(function(){
  $('#prints').popover(
  {
     trigger: 'hover',
     html: true,
     placement: 'bottom',
     content: 'Our prints are supplied on the finest quality papers available on the market today. Prints can be supplied (mounted only) in museum standard archival mounts or framed in a natural ash or a smooth black frame. The mount is signed and numbered on the front.'
  });
});


