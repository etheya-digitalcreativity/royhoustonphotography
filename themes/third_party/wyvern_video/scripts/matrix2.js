(function($) {

    // Display
    var onDisplay = function(cell){
    
        var data = cell.dom.$td;

        var id = cell.field.id+'_'+cell.row.id+'_'+cell.col.id+'_'+Math.floor(Math.random()*100000000);

        // Clean up the class names by removing [],  otherwise our selectors can't freakin find the targets
        $(data).find('*').each(function(){
            var ele_css = $(this).attr('class');
            var ele_rel = $(this).attr('rel');

            if (ele_css)
            {
                var ele_new_css = ele_css.replace(/\[/g, '').replace(/\]/g, '');
                $(this).attr('class', ele_new_css);
            }

            if (ele_rel)
            {
                var ele_new_rel = ele_rel.replace(/\[/g, '').replace(/\]/g, '');
                $(this).attr('rel', ele_new_rel);
            }
        });

        // Duplicate our settings. Settings saved to obj with the col_id as the property, 
        // but Matrix creates funky IDs, so dupe the object so the plugin.js can find the data.
        wyvern_config[id] = wyvern_config['wyvern_video'][cell.col.id];
    };
    
    // Make sure contents are restored after sorting
    // var beforeSort = function(cell)
    // {
        // var $textarea = $('textarea', cell.dom.$td);
        // contents = $('iframe:first', cell.dom.$td)[0].contentDocument.body.innerHTML;
        // $textarea.val(contents);
    // }
    
    // var afterSort = function(cell)
    // {
        // $textarea = $('textarea', cell.dom.$td);
        // cell.dom.$td.empty().append($textarea);
        // onDisplay(cell);
    // }

    Matrix.bind('wyvern_video', 'display', onDisplay);
    // Matrix.bind('wyvern_video', 'beforeSort', beforeSort);
    // Matrix.bind('wyvern_video', 'afterSort', afterSort);

})(jQuery);