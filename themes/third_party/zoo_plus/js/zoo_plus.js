function initPlusList(field_id, selection_type, settings)
{

	$("#selected_entries_ul"+field_id).sortable({
		handle: '.drag',
		revert: true,
		scroll: true, 
		scrollSensitivity: 10
	});
	
	$("#selected_entries_ul"+field_id+" > li > .remove").live('click', function() {
		
		enableInSelect( $(this).parent().find(".inputvalue").val(), field_id);
		$(this).parent().remove();
		return false;
		
	});
	
	$( "ul, li" ).disableSelection();
	
	$("#channel_entries"+field_id).chosen(settings);

	$("#channel_entries"+field_id).change(function() {
		value = $(this).val();
		text = $("#channel_entries"+field_id+" option:selected").text();
		$("#selected_entries"+field_id).append($("<option selected></option>").attr("value",value).text(text));
		addToList(field_id, value, text, selection_type);
	});

}

function addToList(field_id, entry_id, text, selection_type)
{
	if(entry_id != '')
	{
		li = '<li class="clearfix"><div class="drag">&nbsp;'+text+'<input class="inputvalue" name="field_id_'+field_id+'[selected][]" type="hidden" value="'+entry_id+'" /></div><a href="#" class="remove">&nbsp;</a></li>'
		
		if(selection_type == 'multiple')
		{
			$("#selected_entries_ul"+field_id).prepend(li);
			$("#selected_entries_ul"+field_id+" li:first").hide().fadeIn("fast");
			
			$("#channel_entries"+field_id+" option:selected").removeAttr("selected");
			disableInSelect(entry_id, field_id)
		}
		else
		{
			$("#selected_entries_ul"+field_id).html(li);
		}
	}
}

function disableInSelect(entry_id, field_id){
	$("#channel_entries"+field_id+" option[value='"+entry_id+"']").attr('disabled','disabled');	
	$("#channel_entries"+field_id).trigger("liszt:updated");
}

function enableInSelect(entry_id, field_id){
	$("#channel_entries"+field_id+" option[value='"+entry_id+"']").attr('disabled','').removeAttr("disabled");	
	$("#channel_entries"+field_id).trigger("liszt:updated");
}