<?php

/**
 * ExpressionEngine Wyvern Video Module Class
 *
 * @package     ExpressionEngine
 * @subpackage  Modules
 * @category    Wyvern Video
 * @author      Brian Litzinger
 * @copyright   Copyright (c) 2010, 2011 - Brian Litzinger
 * @link        http://boldminded.com/add-ons/wyvern-video
 * @license
 *
 * Copyright (c) 2011, 2012. BoldMinded, LLC
 * All rights reserved.
 *
 * This source is commercial software. Use of this software requires a
 * site license for each domain it is used on. Use of this software or any
 * of its source code without express written permission in the form of
 * a purchased commercial or other license is prohibited.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 * As part of the license agreement for this software, all modifications
 * to this source must be submitted to the original author for review and
 * possible inclusion in future releases. No compensation will be provided
 * for patches, although where possible we will attribute each contribution
 * in file revision notes. Submitting such modifications constitutes
 * assignment of copyright to the original author (Brian Litzinger and
 * BoldMinded, LLC) for such modifications. If you do not wish to assign
 * copyright to the original author, your license to  use and modify this
 * source is null and void. Use of this software constitutes your agreement
 * to this clause.
 */

require_once PATH_THIRD.'wyvern_video/config.php';

class wyvern_video {

    public $settings;
    private $params;
    private $cache_path;
    private $cache;
    private $service;

    const MAX_RESULTS = 20;
    const CACHE_KEY = 'wyvern_video/';
    const CACHE_TTL = 3600; // 1 hour

    public function __construct()
    {
        // Create cache
        if (! isset(ee()->session->cache[WYVERN_VIDEO_SHORT_NAME]))
        {
            ee()->session->cache[WYVERN_VIDEO_SHORT_NAME] = array();
        }
        $this->cache =& ee()->session->cache[WYVERN_VIDEO_SHORT_NAME];

        if (! isset(ee()->wyvern_video_helper))
        {
            require PATH_THIRD.'wyvern_video/helper.wyvern_video.php';
            ee()->wyvern_video_helper = new Wyvern_video_helper;
        }

        // ee()->load->library('pagination');
        // $this->pagination = new Pagination_object(__CLASS__);
        // $this->pagination->dynamic_sql = FALSE;
        // $this->pagination->get_template();

        $this->settings = ee()->wyvern_video_helper->get_settings();

        $this->cache_path = APPPATH . 'cache/wyvern_video/';

        // Make sure our cache folder exists.
        if ( ! is_dir($this->cache_path))
        {
            mkdir($this->cache_path, DIR_WRITE_MODE, TRUE);
        }
    }

    private function _get_params()
    {
        $this->params['settings']   = $this->settings['settings_'. $this->service];
        $this->params['username']   = ee()->TMPL->fetch_param('user');
        $this->params['prefix']     = ee()->TMPL->fetch_param('variable_prefix', '');

        $this->params['width']      = ee()->TMPL->fetch_param('width', 360);
        $this->params['height']     = ee()->TMPL->fetch_param('height', 240);

        $this->params['query']      = ee()->TMPL->fetch_param('query');
        $this->params['limit']      = ee()->TMPL->fetch_param('limit', self::MAX_RESULTS);
        $this->params['offset']     = ee()->TMPL->fetch_param('offset', 1);
    }

    /**
     * The template module tag for displaying a list of videos from YouTube based on a query.
     *
     * {exp:wyvern_video:youtube}
     *      {video_id}
     *      {title}
     *      {content} or {description}
     *      {url}
     *      {url_mobile}
     *      {thumbnail_0} {thumbnail_1} {thumbnail_2} {thumbnail_3}
     *      {author}
     *      {category}
     *      {keywords}
     *          {keyword}
     *      {/keywords}
     *      {duration}
     *      {view_count}
     *      {favorite_count}
     *      {id} - e.g. http://gdata.youtube.com/feeds/api/videos/[video id]
     *      {published}
     *      {updated}
     *      {embed}
     * {/exp:wyvern_video:youtube}
     */
    public function youtube()
    {
        $videos = array();
        $this->service = 'youtube';
        $this->_get_params();

        // Avoid conflict with the REST module
        if ( ! isset(ee()->curl))
        {
            ee()->load->library('curl');
        }

        ee()->TMPL->log_item('Wyvern Video: Querying YouTube');

        $query_params = array(
            'q' => $this->params['query'],
            'maxResults' => $this->params['limit']
        );

        if (isset($this->params['username']) && $this->params['username'] != '') {
            $query_params['forUsername'] = $this->params['username'];
        }

        if (!isset($query_params['q'])) {
            ee()->TMPL->log_item('Wyvern Video: A query parameter is required to search YouTube.');
        }

        $prefix = $this->params['prefix'];
        $data = $this->load_youtube($query_params, TRUE);

        if ( !$data || count($data['videos']) === 0)
        {
            ee()->TMPL->log_item('Wyvern Video: No Results found for '. $url);
            return ee()->TMPL->no_results();
        }

        $k = 0;
        foreach($data['videos'] as $video)
        {
            foreach ($video as $index => $value)
            {
                $videos[$k][$prefix.$index] = $value;
            }

            // finally add the full embed code...
            $videos[$k][$prefix.'embed'] = '<iframe src="http://www.youtube.com/embed/'. $videos[$k][$prefix.'video_id'] .'?rel=0&wmode=opaque&hd=1" border="0" width="'. $this->params['width'] .'" height="'. $this->params['height'] .'"></iframe>';

            $k++;
        }

        return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $videos);
    }


    /**
     * The template module tag for displaying a list of videos from Vimeo based on a query.
     *
     * {exp:wyvern_video:vimeo}
     *      {video_id}
     *      {title}
     *      {content} or {description}
     *      {url}
     *      {url_mobile}
     *      {thumbnail_0} {thumbnail_1} {thumbnail_2} {thumbnail_3}
     *      {author}
     *      {tags}
     *          {tag}
     *      {/tags}
     *      {duration}
     *      {view_count}
     *      {favorite_count}
     *      {id}
     *      {published}
     *      {updated}
     *      {embed}
     * {/exp:wyvern_video:vimeo}
     */
    public function vimeo()
    {
        $videos = array();
        $this->service = 'youtube';
        $this->_get_params();

        $url = 'http://vimeo.com/api/rest/v2';

        // The URL is not actually used in this class for Vimeo,
        // the Vimeo lib handles it, but adding here for logging purposes.
        if ($this->params['username'])
        {

            $url .= $this->params['username'] .'?user_id='. $this->params['username'] .'&query='. $this->params['query'] .'&per_page='. $this->params['limit'] .'&page='. $this->params['offset'];
        }
        else
        {
            $url .= $this->params['username'] .'?query='. $this->params['query'] .'&per_page='. $this->params['limit'] .'&page='. $this->params['offset'];
        }

        ee()->TMPL->log_item('Wyvern Video: Querying '. $url);
        $this->params['url'] = $url;
        $prefix = $this->params['prefix'];

        try
        {
            $data = $this->load_vimeo('videos.search', array(
                'per_page'  => $this->params['limit'],
                'page'      => $this->params['offset'],
                'query'     => $this->params['query'],
                'user_id'   => $this->params['username']
            ), TRUE);

            if ( ! $data OR count($data->videos->video) == 0)
            {
                return ee()->TMPL->no_results();
            }

            foreach ($data->videos->video as $k => $entry)
            {
                $videos[$k][$prefix.'title'] = $entry->title;

                // get the video details/info
                $video_info = $this->load_vimeo('videos.getInfo', array(
                    'video_id'   => $entry->id
                ), TRUE);

                $video = $video_info->video[0];

                // get basic info
                $videos[$k][$prefix.'id']               = $video->id;
                $videos[$k][$prefix.'video_id']         = $video->id;
                $videos[$k][$prefix.'content']          = $video->description;
                $videos[$k][$prefix.'description']      = $video->description;
                $videos[$k][$prefix.'published']        = strtotime($video->upload_date);
                $videos[$k][$prefix.'updated']          = strtotime($video->modified_date);
                $videos[$k][$prefix.'view_count']       = $video->number_of_plays;
                $videos[$k][$prefix.'favorite_count']   = $video->number_of_likes;
                $videos[$k][$prefix.'duration']         = $video->duration;
                $videos[$k][$prefix.'author']           = $video->owner->display_name;

                foreach ($video->tags->tag as $tag)
                {
                    $videos[$k][$prefix.'tags'][] = array($prefix.'tag' => $tag->normalized);
                }

                // get video thumbnails
                foreach ($video->thumbnails->thumbnail as $thb_key => $thumbnail)
                {
                    $videos[$k][$prefix.'thumbnail_'. $thb_key] = $thumbnail->_content;
                }

                // get the full URLs
                foreach ($video->urls->url as $vid_key => $url)
                {
                    if ($url->type == 'mobile')
                    {
                        $videos[$k][$prefix.'url_mobile'] = $url->_content;
                    }
                    else
                    {
                        $videos[$k][$prefix.'url'] = $url->_content;
                    }
                }

                // finally add the full embed code...
                $videos[$k][$prefix.'embed'] = '<iframe src="http://player.vimeo.com/video/'. $videos[$k][$prefix.'video_id'] .'?title=0&byline=0&portrait=0" border="0" width="'. $this->params['width'] .'" height="'. $this->params['height'] .'"></iframe>';
            }
        }
        catch (Exception $e)
        {
            ee()->TMPL->log_item('Wyvern Video: Could not load '. $this->params['url']);
        }

        return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $videos);
    }

    /**
     * Search Vimeo, and return response.
     * Called only via an ACT request or from $this->vimeo()
     *
     * @param bool $method
     * @param array $params
     * @param bool $return
     * @return array
     */
    public function load_vimeo($method = FALSE, $params = array(), $return = FALSE)
    {
        @session_start();

        require_once PATH_THIRD .'/wyvern_video/libraries/vimeo/vimeo.php';

        $vimeo = new phpVimeo(ee()->wyvern_video_helper->vimeo_key, ee()->wyvern_video_helper->vimeo_secret);
        $vimeo->enableCache(phpVimeo::CACHE_FILE, $this->cache_path, 300);

        // Set up variables
        $state = isset($_SESSION['vimeo_state']) ? $_SESSION['vimeo_state'] : FALSE;
        $request_token = isset($_SESSION['oauth_request_token']) ? $_SESSION['vimeo_state'] : FALSE;
        $access_token = isset($_SESSION['oauth_access_token']) ? $_SESSION['vimeo_state'] : FALSE;

        // Coming back
        if (ee()->input->get('oauth_token') AND $state === 'start') {
            $_SESSION['vimeo_state'] = $state = 'returned';
        }

        // If we have an access token, set it
        if ($access_token) {
            $vimeo->setToken($access_token, $_SESSION['oauth_access_token_secret']);
        }

        $state = isset($_SESSION['vimeo_state']) ? $_SESSION['vimeo_state'] : $state;

        switch ($state)
        {
            default:
                // Get a new request token
                $token = $vimeo->getRequestToken();

                // Store it in the session
                $_SESSION['oauth_request_token'] = $token['oauth_token'];
                $_SESSION['oauth_request_token_secret'] = $token['oauth_token_secret'];
                $_SESSION['vimeo_state'] = 'start';

                // Build authorize link
                $authorize_link = $vimeo->getAuthorizeUrl($token['oauth_token'], 'read');

            break;

            case 'returned':

                // Store it
                if ($_SESSION['oauth_access_token'] === NULL && $_SESSION['oauth_access_token_secret'] === NULL) {
                    // Exchange for an access token
                    $vimeo->setToken($_SESSION['oauth_request_token'], $_SESSION['oauth_request_token_secret']);
                    $token = $vimeo->getAccessToken($_REQUEST['oauth_verifier']);

                    // Store
                    $_SESSION['oauth_access_token'] = $token['oauth_token'];
                    $_SESSION['oauth_access_token_secret'] = $token['oauth_token_secret'];
                    $_SESSION['vimeo_state'] = 'done';

                    // Set the token
                    $vimeo->setToken($_SESSION['oauth_access_token'], $_SESSION['oauth_access_token_secret']);
                }

                // Do an authenticated call
                try
                {
                    $videos = $vimeo->call('vimeo.videos.getUploaded');
                }
                catch (VimeoAPIException $e)
                {
                    $msg = "Encountered Vimeo API error -- code {$e->getCode()} - {$e->getMessage()}";

                    if ($method)
                    {
                        ee()->TMPL->log_item('Wyvern Video: '. $msg);
                    }
                    else
                    {
                        echo $msg;
                    }
                }

            break;
        }

        // Get extra params from $GET
        if ($user_id = ee()->input->get('user_id'))
        {
            $params['user_id'] = $user_id;
        }

        $method = $method ? $method : ee()->input->get('method');

        switch($method)
        {
            case "videos.search":
                $data = $vimeo->call('vimeo.videos.search', array_merge(array(
                    'query' => ee()->input->get('query'),
                    'per_page' => ee()->input->get('per_page'),
                    'page' => (ee()->input->get('page') ? ee()->input->get('page') : 1)
                ), $params));
            break;

            case "videos.getInfo":
                $data = $vimeo->call('vimeo.videos.getInfo', array_merge(array(
                    'video_id' => ee()->input->get('video_id')
                ), $params));
            break;

            case "videos.getAll":
                $data = $vimeo->call('vimeo.videos.getAll', array_merge(array(
                    'user_id' => ee()->input->get('user_id')
                ), $params));
            break;
        }

        if ($return)
        {
            return $data;
        }
        else
        {
            $this->send_json_response($data);
        }
    }

    /**
     * @param array $params
     * @param bool $return Return as JSON (default)? If true will return PHP array instead.
     * @return array
     */
    public function load_youtube($params = array(), $return = FALSE)
    {
        if (!isset($this->settings['settings_youtube']['api_key']) || $this->settings['settings_youtube']['api_key'] == '') {
            show_error('Please enter your Google API Key in the Wyvern Video module settings page to search YouTube.');
        }

        $apiKey = $this->settings['settings_youtube']['api_key'];

        require_once 'libraries/google/apiclient/src/Google/autoload.php';

        $client = new Google_Client();
        $client->setApplicationName("Wyvern Video");
        // AIzaSyCfL94qWOLWxVCoPon1zPW9UyhX5Du27ZQ
        $client->setDeveloperKey($apiKey);

        $params = array_merge(array(
            'type' => 'video',
            'pageToken' => ee()->input->get('pageToken') ?: null,
            'maxResults' => ee()->input->get('maxResults') ?: self::MAX_RESULTS,
            'q' => ee()->input->get('q')
        ), $params);

        $cache_key = md5(serialize($params));

        if (version_compare(APP_VER, '2.9', '>') && ($cache_results = ee()->cache->get($cache_key)) !== FALSE)
        {
            if ($return)
            {
                return $cache_results;
            }
            else
            {
                $this->send_json_response($cache_results);
            }
        }

        $service = new Google_Service_YouTube($client);
        $searchResults = $service->search->listSearch('id,snippet', $params);

        $response = array(
            'nextPage' => $searchResults->nextPageToken,
            'prevPage' => $searchResults->prevPageToken,
            'totalResults' => $searchResults->getPageInfo()->getTotalResults(),
            'videos' => array()
        );

        $videoIds = array();
        $details = array();

        foreach ($searchResults as $item) {
            $videoIds[] = $item->getId()->getVideoId();
        }

        $parts = 'snippet,contentDetails,statistics,status';
        // ,fileDetails,statistics,processingDetails,recordingDetails,topicDetails
        $videoListResults = $service->videos->listVideos($parts, array(
            'id' => implode(',', $videoIds)
        ));

        /* @var Google_Service_YouTube_Video $video */
        foreach ($videoListResults as $video) {
            /* @var Google_Service_YouTube_VideoContentDetails $content */
            $content = $video->getContentDetails();
            /* @var Google_Service_YouTube_VideoStatistics $statistics */
            $statistics = $video->getStatistics();
            /* @var Google_Service_YouTube_VideoStatus $status */
            $status = $video->getStatus();
            /* @var Google_Service_YouTube_VideoSnippet $snippet */
            $snippet = $video->getSnippet();
            /* @var Google_Service_YouTube_ThumbnailDetails $thumbnails */
            $thumbnails = $snippet->getThumbnails();

            $length = new DateInterval($content->getDuration());
            $length = $length->format('%H:%I:%S');

            $defaultThumbnail = $thumbnails->getDefault();
            $mediumThumbnail = $thumbnails->getMedium();
            $highThumbnail = $thumbnails->getHigh();
            $standardThumbnail = $thumbnails->getStandard();
            $maxThumbnail = $thumbnails->getMaxres();

            $details[$video->getId()] = array(
                'type' => 'youtube',
                'title' => $snippet->getTitle(),
                'description' => $snippet->getDescription(),
                'content' => $snippet->getDescription(),
                'author' => $snippet->getChannelTitle(),
                'duration' => $content->getDuration(),
                'length' => $length,
                'channel_id' => $snippet->getChannelId(),
                'dimension' => $content->getDimension(),
                'licensed_content' => $content->getLicensedContent(),
                'caption' => $content->getCaption(),
                'definition' => $content->getDefinition(),
                'view_count' => $statistics->getViewCount(),
                'favorite_count' => $statistics->getFavoriteCount(),
                'comment_count' => $statistics->getCommentCount(),
                'dislike_count' => $statistics->getDislikeCount(),
                'category' => $snippet->getCategoryId(),
                'published' => $snippet->getPublishedAt(),
                'thumbnail_default' => isset($defaultThumbnail->url) ? $defaultThumbnail->url : null,
                'thumbnail_medium' => isset($mediumThumbnail->url) ? $mediumThumbnail->url : null,
                'thumbnail_high' => isset($highThumbnail->url) ? $highThumbnail->url : null,
                'thumbnail_standard' => isset($standardThumbnail->url) ? $standardThumbnail->url : null,
                'thumbnail_max' => isset($maxThumbnail->url) ? $maxThumbnail->url : null,

                // Legacy variables
                'thumbnail_0' => isset($defaultThumbnail->url) ? $defaultThumbnail->url : null,
                'thumbnail_1' => isset($mediumThumbnail->url) ? $mediumThumbnail->url : null,
                'thumbnail_2' => isset($highThumbnail->url) ? $highThumbnail->url : null,
                'thumbnail_3' => isset($standardThumbnail->url) ? $standardThumbnail->url : null,
                'thumbnail_4' => isset($maxThumbnail->url) ? $maxThumbnail->url : null,
            );
        }

        foreach ($searchResults as $video) {
            $videoId = $video->getId()->getVideoId();

            $row = array(
                'video_id' => $videoId,
                'url' => '//www.youtube.com/watch?v='.$videoId,
                'url_mobile' => '//m.youtube.com/watch?v='.$videoId,
            );

            if (isset($details[$videoId])) {
                $row = array_merge($row, $details[$videoId]);
            }

            $response['videos'][] = $row;
        }

        if (version_compare(APP_VER, '2.9', '>'))
        {
            ee()->cache->save($cache_key, $response, self::CACHE_TTL);
        }

        if ($return)
        {
            return $response;
        }
        else
        {
            $this->send_json_response($response);
        }
    }

    /**
     * @param $response
     * @return string
     */
    private function send_json_response($response)
    {
        ee()->output->enable_profiler(FALSE);
        @header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($response);
        exit;
    }

    /**
     * @return bool
     */
    public function  isSecure() {
        return
            (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
            || $_SERVER['SERVER_PORT'] == 443;
    }


    /**
     * Cache a response.
     *
     * @param array $params The parameters for the response.
     * @param string $response The serialized response data.
     */
    private function _set_cache($response)
    {
        $hash = md5(serialize($this->params));

        $file = reduce_double_slashes($this->cache_path.'/'.$hash.'.cache');

        if (file_exists($file))
        {
            unlink($file);
        }

        return file_put_contents($file, $response);
    }

    /**
     * Get the unserialized contents of the cached request.
     *
     * @param array $params The full list of api parameters for the request.
     */
    private function _get_cache()
    {
        $hash = md5(serialize($this->params));

        $file = reduce_double_slashes($this->cache_path.'/'.$hash.'.cache');

        if (file_exists($file))
        {
            ee()->TMPL->log_item('Wyvern Video: Loading '. $this->service .' data from cache '. $file);
            return file_get_contents($file);
        }

        return FALSE;
    }

}