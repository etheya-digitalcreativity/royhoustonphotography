<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
 <?php echo $form_edit; ?>
	
	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead class="">
			<tr>
				<th colspan="2">
					<strong><?=lang($module_name.'_add')?> </strong><br />
					<?=lang($module_name.'_add_description')?>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<?=lang($module_name.'_label') ?> 
 				</td>
				<td style='width:50%;'>
 					<input  dir='ltr' type='text' name='label'  value='' size='90' maxlength='250' />
				</td>
 			</tr>
			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<?=lang($module_name.'_short_name') ?> 
 				</td>
				<td style='width:50%;'>
 					<input  dir='ltr' type='text' name='short_name'  value='' size='90' maxlength='250' />
				</td>
 			</tr>
 			<tr class="<?php echo alternator('even', 'odd');?>">
				<td colspan="2">
					<?=$list?>
				</td>
 			</tr>
		</tbody>
	</table>

	<input type="hidden" value="add" name="action" /> 
	
	<p><input type="submit" name="submit" value="<?=lang('submit')?>" class="submit" /></p>
	
</form>
 