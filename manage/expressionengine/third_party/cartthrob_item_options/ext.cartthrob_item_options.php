<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property CI_Controller $EE
 * @property Cartthrob_cart $cart
 * @property Cartthrob_store $store
 */
class Cartthrob_item_options_ext
{
	public $module_name = "cartthrob_item_options"; 
	public $settings = array();
	public $name = 'CartThrob Addon'; // Generic name is replaced later with config name
	public $version;
	public $description = ''; // Generic description is replaced using lang files
	public $settings_exist = 'y';
	public $docs_url = 'http://cartthrob.com/docs_global_item_options/';
	public $required_by = array('module');
	
	// @NOTE: Extension hooks are published by UPD
	
	/**
	* Cartthrob_ext
	*/
	public function __construct($settings='')
	{
		$this->EE =& get_instance();
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/'); 
		include PATH_THIRD.$this->module_name.'/config'.EXT;
		$this->name= $config['name']; 
		$this->version = $config['version'];
		$this->description = lang($this->module_name. "_description"); 
		$this->EE->load->helper(array( 'data_formatting'));
		
	}
	
	public function settings_form()
	{
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name);
	}
	
	/**
	 * Activates Extension
	 *
	 * @access public
	 * @param NULL
	 * @return void
	 * @since 1.0.0
	 * @author Rob Sanchez
	 */
	public function activate_extension()
	{
		return TRUE;
	}
	// END
	
	
	// --------------------------------
	//  Update Extension
	// --------------------------------  
	/**
	 * Updates Extension
	 *
	 * @access public
	 * @param string
	 * @return void|BOOLEAN False if the extension is current
	 * @since 1.0.0
	 * @author Rob Sanchez
	 */
	public function update_extension($current='')
	{
		if ($current == '' OR $current == $this->version)
		{
			return FALSE;
		}
		
		$this->EE->db->update('extensions', array('version' => $this->version), array('class' => __CLASS__));
		
		return TRUE;
	}
	// END
	
	// --------------------------------
	//  Disable Extension
	// --------------------------------
	/**
	 * Disables Extension
	 * 
	 * Deletes mention of this extension from the exp_extensions database table
	 *
	 * @access public
	 * @param NULL
	 * @return void
	 * @since 1.0.0
	 * @author Rob Sanchez
	 */
	public function disable_extension()
	{
		$this->EE->db->delete('extensions', array('class' => __CLASS__));
	}
	// END
	
	// --------------------------------
	//  Settings Function
	// --------------------------------
	/**
	 * @access public
	 * @param NULL
	 * @return void
	 * @since 1.0.0
	 * @author Rob Sanchez
	 */
	public function settings()
	{
	}
	// END
 	public function cartthrob_add_settings_nav($nav = array())
	{
		// @TODO maybe re-add this. 
		
		#$nav[$this->module_name] = array($this->module_name => 'nav_'.$this->module_name); 

		return $nav;
	}
 
   	public function cartthrob_get_all_price_modifiers($entry_id)
	{
		$this->EE->load->library('get_settings');
		$settings = $this->EE->get_settings->settings($this->module_name);
		$price_modifiers = array(); 
		
		$this->EE->load->model('generic_model');
		$model = new Generic_model("cartthrob_item_options_options");
		
		$this->EE->load->model('cartthrob_entries_model'); 
		$this->EE->load->model('cartthrob_field_model'); 
		$entry_data = $this->EE->cartthrob_entries_model->entry($entry_id); 
		$group = $entry_data['field_group']; 
		$field = $this->EE->cartthrob_field_model->channel_has_fieldtype($entry_data['channel_id'], "cartthrob_item_options_select", $return_field_id = TRUE); 
		
		$available_options = array(); 
		if ($field)
		{
			$available_options = $entry_data['field_id_'.$field];
			if (!empty($available_options))
			{
				$available_options = explode( "|", $available_options); 
			}
		}
		
		if (!empty($settings['global_item_options_channels']))
		{
			$gio =  $settings['global_item_options_channels']; 
			$cid = $entry_data['channel_id']; 
			if (in_array("none", $gio) || (!in_array($cid, $gio)) && !in_array("all", $gio))
			{
				return $price_modifiers; 
			}
		}
		$group_field = $this->EE->cartthrob_field_model->channel_has_fieldtype($entry_data['channel_id'], "cartthrob_item_groups_select", $return_field_id = TRUE); 
		
		$available_groups = array(); 
		if ($group_field)
		{
			$available_groups = $entry_data['field_id_'.$group_field];
			if (!empty($available_groups))
			{
				$available_groups = explode( "|", $available_groups); 
			}
		}
		
		$limit="200";
		$offset="0"; 
		
		$option_groups = $model->read($id=NULL,$order_by=NULL,$order_direction='asc',$field_name=NULL,$string=NULL,$limit,$offset); 
		
		$default_keys = array(
			'option_name' => '',
			'option_value' => '',
			'price' => 0,
		);
 		
		if ($option_groups)
		{
			$this->EE->cartthrob->cart->set_meta("global_item_options", $option_groups); 
			
			foreach ($option_groups as $key => $option_data)
			{
				$short_name 	= $this->arr($option_data, "short_name");
				$data		 = $this->arr($option_data, "data");
				
				if (!$short_name || !$data)
				{
					continue;
				}
				
				$item_option_labels = $this->EE->cartthrob->cart->meta('item_option_labels'); 
				$item_option_labels[$short_name] = $this->arr($option_data,"label"); 
 				$this->EE->cartthrob->cart->set_meta('item_option_labels', $item_option_labels);
				
				// adding default keys to all returned values. 
				$new_data = array();
				$data = _unserialize($data); 

				if (!empty($available_options))
				{
	 				foreach ($data as $key=>$value)
					{
						if (in_array($value['option_value'], $available_options ))
						{
							$new_data[$key] = array_merge($default_keys, $value);
						}
 					}
 				}
				elseif (!empty($available_groups))
				{
 					foreach ($data as $key=>$value)
					{
						$new_data[$key] = array_merge($default_keys, $value);
 					}
				}
				else
				{
					foreach ($data as $key=>$value)
					{
						$new_data[$key] = array_merge($default_keys, $value);
 					}
				}
				

				if (!empty($available_groups))
				{
					if (in_array($short_name, $available_groups))
					{
						$price_modifiers[$short_name] = $new_data; 
					}
				}
				else
				{
 					$price_modifiers[$short_name] = $new_data; 
				}
			}
 		}
 
		if ($this->EE->extensions->last_call)
		{
			$existing_modifiers = $this->EE->extensions->last_call;  
			if(is_array($existing_modifiers))
			{
				$price_modifiers = array_merge($existing_modifiers, $price_modifiers); 
			}
		}
		
   		return $price_modifiers; 
	}
	
	/**
	 * arr
	 *
	 * @param array $array 
	 * @param string $key 
	 * @return string|null
	 * @author Chris Newton
	 * 
	 * Checks an array for a key, returns it if set, or NULL if not set.
	 */
	function arr($array, $key)
	{
		if (isset($array[$key]))
		{
			return $array[$key]; 
		}
		else
		{
			return NULL; 
		}
	}
}
// END CLASS
