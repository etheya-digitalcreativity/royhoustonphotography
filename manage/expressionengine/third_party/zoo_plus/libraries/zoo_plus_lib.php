<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD . 'zoo_plus/helpers/zoo_plus_helper.php';

class Zoo_plus_lib
{

	/**
	 * Constructor
	 *
	 * @access public
	 */
	function Zoo_plus_lib()
	{
		// Creat EE Instance
		$this->EE =& get_instance();
		$this->EE->load->model("member_model");
		$this->EE->load->library("logger");
		$this->zoo_settings = get_zoo_settings($this->EE);

	}

	function get_visitor_id($member_id = 'current')
	{


		if ($member_id == 'current' || $member_id == '') {
			if (!isset($this->EE->session)) {
				//no session is available, this will handle trying to get the gloval var zoo_visitor_id when user is not logged in;
				$member_id = 0;
			} else {
				$member_id = $this->EE->session->userdata['member_id'];
			}
		}
		if ($member_id == 0) {
			return FALSE;
		}

		if (isset($this->zoo_settings['member_channel_id'])) {
			$visitor_query = $this->EE->db->select('entry_id')->where('author_id', $member_id)->where('channel_id', $this->zoo_settings['member_channel_id'])->order_by('entry_id', 'desc')->limit(1)->get('channel_titles');
			if ($visitor_query->num_rows() > 0) {
				return $visitor_query->first_row()->entry_id;
			} else
			{
				return FALSE;
			}
		}
	}

	function get_visitor_id_by_username($username = '')
	{


		if ($username == '') {
			return FALSE;
		}

		if (isset($this->zoo_settings['member_channel_id'])) {

			$visitor_query = $this->EE->db->query("SELECT ct.entry_id FROM " . $this->EE->db->dbprefix . "members m, " . $this->EE->db->dbprefix . "channel_titles as ct WHERE m.username = '" . $username . "' and ct.author_id = m.member_id AND ct.channel_id = '" . $this->zoo_settings['member_channel_id'] . "'");
			if ($visitor_query->num_rows() > 0) {
				return $visitor_query->first_row()->entry_id;
			} else
			{
				return FALSE;
			}
		}
	}

	function check_relation($field_name, $parent_id, $child_id)
	{


		$field_settings = get_plus_settings($this->EE, $field_name);

		if ($field_settings) {
			$direction = $field_settings['plus_relation_direction'];

			$parent_field_id = ($direction == 'reverse') ? $field_settings['plus_field_id'] : $field_settings['field_id'];
			$parent_entry_id = ($direction == 'reverse') ? $child_id : $parent_id;
			$child_entry_id  = ($direction == 'reverse') ? $parent_id : $child_id;

			$sql       = 'SELECT relation_id FROM exp_zoo_plus_relationships WHERE parent_id = "' . $parent_entry_id . '" AND child_id = "' . $child_entry_id . '" AND parent_field_id = "' . $parent_field_id . '"';
			$rel_query = $this->EE->db->query($sql);


			if (isset($rel_query) && $rel_query->num_rows() > 0) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}

	}

	function total_relation($field_name, $entry_id)
	{

		$field_settings = get_plus_settings($this->EE, $field_name);

		if ($field_settings) {
			$direction = $field_settings['plus_relation_direction'];

			if ($direction == 'to_channel') {

				$this->EE->db->select('relation_id');
				$this->EE->db->where('parent_field_id', $field_settings['field_id']);
				$this->EE->db->where('child_id', $entry_id);
				$rel_query = $this->EE->db->get('zoo_plus_relationships');

			}

			if ($direction == 'reverse') {

				$this->EE->db->select('relation_id');
				$this->EE->db->where('parent_field_id', $field_settings['plus_field_id']);
				$this->EE->db->where('parent_id', $entry_id);
				$rel_query = $this->EE->db->get('zoo_plus_relationships');

			}

		}

		return (isset($rel_query)) ? $rel_query->num_rows() : 0;

	}

	// ================
	// = Add relation =
	// ================
	function add_relation($field_name, $parent_id, $child_id)
	{

		$field_settings = get_plus_settings($this->EE, $field_name);

		if ($field_settings) {
			$direction = $field_settings['plus_relation_direction'];

			$parent_field_id = ($direction == 'reverse') ? $field_settings['plus_field_id'] : $field_settings['field_id'];
			$parent_entry_id = ($direction == 'reverse') ? $child_id : $parent_id;
			$child_entry_id  = ($direction == 'reverse') ? $parent_id : $child_id;

			$field_type = 'plus';

			$reldata = array('parent_id'       => $parent_entry_id,
			                 'parent_field_id' => $parent_field_id,
			                 'child_id'        => $child_entry_id
			);

			$table = 'zoo_plus_relationships';
		}

		if ($this->EE->db->insert($table, $reldata)) {
			// =============================
			// = Add relation end add HOOK =
			// =============================
			$member_id = $this->EE->session->userdata['member_id'];
			$hook_data = array('parent_id'  => $parent_entry_id,
			                   $member_id   => $member_id,
			                   'child_id'   => $child_entry_id,
			                   'field_id'   => $parent_field_id,
			                   'field_type' => $field_type);

			if ($this->EE->extensions->active_hook('zoo_plus_add_relation_end') === TRUE) {
				$edata = $this->EE->extensions->call('zoo_plus_add_relation_end', $hook_data);
				if ($this->EE->extensions->end_script === TRUE) return;
			}


			return TRUE;
		} else {
			return FALSE;
		}

	}

	// ===================
	// = Remove relation =
	// ===================
	function remove_relation($field_name, $parent_id, $child_id)
	{

		$field_settings = get_plus_settings($this->EE, $field_name);

		if ($field_settings) {
			$direction = $field_settings['plus_relation_direction'];

			$parent_field_id = ($direction == 'reverse') ? $field_settings['plus_field_id'] : $field_settings['field_id'];
			$parent_entry_id = ($direction == 'reverse') ? $child_id : $parent_id;
			$child_entry_id  = ($direction == 'reverse') ? $parent_id : $child_id;

			$field_type = "plus";

			$reldata = array('parent_id'       => $parent_entry_id,
			                 'parent_field_id' => $parent_field_id,
			                 'child_id'        => $child_entry_id
			);

			$table = 'zoo_plus_relationships';

			if ($this->EE->db->delete($table, $reldata)) {
				// ============================
				// = Remove relation end HOOK =
				// ============================
				$member_id = $this->EE->session->userdata['member_id'];
				$hook_data = array('parent_id'  => $parent_entry_id,
				                   $member_id   => $member_id,
				                   'child_id'   => $child_entry_id,
				                   'field_id'   => $parent_field_id,
				                   'field_type' => $field_type);

				$edata = $this->EE->extensions->universal_call('zoo_plus_remove_relation_end', $hook_data);
				if ($this->EE->extensions->end_script === TRUE) return;

				return TRUE;

			} else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}

	function parse_entries($selected_entries = "0", $fixed_order = false, $prefix = 'plus_prefix')
	{

		if ($selected_entries == "0") {
			//return $this->EE->TMPL->no_results();
		}

		$TMPL_cache = $this->EE->TMPL;

		unset($this->EE->TMPL->tagparams['entry_id']);

		$this->EE->TMPL->tagparams['channel'] = $this->EE->TMPL->fetch_param('channel', '');
		$this->EE->TMPL->tagparams['status'] = $this->EE->TMPL->fetch_param('status', 'not closed');
		$this->EE->TMPL->tagparams['dynamic'] = 'no';
		// $this->EE->TMPL->tagparams['status'] = 'not closed';
		//$this->EE->TMPL->tagparams['show_expired'] = ($selected_entries == "0") ? 'yes' : 'no';
		//$this->EE->TMPL->tagparams['show_future_entries'] = 'yes';
		//$this->EE->TMPL->tagparams['require_entry'] = 'yes';
		// $this->EE->TMPL->tagparams['disable'] = 'categories|category_fields|member_data|pagination';


		if ($fixed_order)
			$this->EE->TMPL->tagparams['fixed_order'] = $selected_entries;
		else
			$this->EE->TMPL->tagparams['entry_id'] = $selected_entries;

		if ($selected_entries == "0") {
			$this->EE->TMPL->tagparams['entry_id'] = "-1";
		}

		$tagdata = $this->EE->TMPL->tagdata;

		$tagdata = str_replace("{visitor:", "{", $tagdata);
		$tagdata = str_replace("{/visitor:", "{/", $tagdata);
		$tagdata = str_replace("{if visitor:", "{if ", $tagdata);
		$tagdata = str_replace("member:", "", $tagdata);
		$tagdata = str_replace("/member:", "/", $tagdata);
		$tagdata = str_replace($prefix . ":", "", $tagdata);
		$tagdata = str_replace("/" . $prefix . ":", "/", $tagdata);

		$tagdata = str_replace("entry:", "", $tagdata);
		$tagdata = str_replace("/entry:", "/", $tagdata);

		$this->EE->TMPL->tagdata = $tagdata;

		//	if (version_compare(APP_VER, '2.1.3', '<'))
		//{

		preg_match("/\{if no_results\}(.*?)\{\/if\}/s", $tagdata, $m);
		if (count($m) > 0) {
			$this->EE->TMPL->no_results_block = $m[0];
			$this->EE->TMPL->no_results       = $m[1];
		}
		else
		{
			$this->EE->TMPL->no_results_block = '';
			$this->EE->TMPL->no_results       = '';
		}
		//	}

		$var_single_cache = $this->EE->TMPL->var_single;
		$var_pair_cache   = $this->EE->TMPL->var_pair;

		// 	
		$vars = $this->EE->functions->assign_variables($tagdata);

		$this->EE->TMPL->var_single = $vars['var_single'];
		$this->EE->TMPL->var_pair   = $vars['var_pair'];

		if (!class_exists('Channel')) {
			require PATH_MOD . 'channel/mod.channel.php';
		}

		// create a new Channel object and run entries()
		$Zoo_Channel = new Channel();
		$entries     = $Zoo_Channel->entries();

		$this->EE->TMPL->var_single = $var_single_cache;
		$this->EE->TMPL->var_pair   = $var_pair_cache;

		$this->EE->TMPL = $TMPL_cache;

		return $entries;


	}

}

?>