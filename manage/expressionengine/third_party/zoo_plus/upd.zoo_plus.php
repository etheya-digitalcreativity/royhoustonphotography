<?php 

if (!defined('BASEPATH')) exit('Invalid file request');
if (!defined('PATH_THIRD')) define('PATH_THIRD', EE_APPPATH.'third_party/');
	require_once PATH_THIRD.'zoo_plus/config.php';

/**
 * Zoo Plus Update Class
 *
 * @package   Zoo Plus
 * @author    ExpressionEngine Zoo <info@eezoo.com>
 * @copyright Copyright (c) 2011 ExpressionEngine Zoo (http://eezoo.com)
 */
class Zoo_plus_upd
{
	var $version = ZOO_PLUS_VER; 
	var $module_name = ZOO_PLUS_CLASS;
	
	function Zoo_plus_upd() 
	{
		// Make a local reference to the ExpressionEngine super object
		$this->EE =& get_instance();
	} 
	
	function install() 
	{
		// Insert module data
		$data = array(
			'module_name' => $this->module_name,
			'module_version' => $this->version,
			'has_cp_backend' => 'n',
			'has_publish_fields' => 'n'
		);
		
		$this->EE->db->insert('modules', $data);
		
		// zoo visitor settings 
		$fields = array(
			'id'			=>	array('type' => 'int', 'constraint' => '10', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
			'site_id'		=>	array('type' => 'int', 'constraint' => '8', 'unsigned' => TRUE, 'null' => FALSE, 'default' => '1'),
			'var'			=>	array('type' => 'varchar', 'constraint' => '60', 'null' => FALSE),
			'var_value'		=>	array('type' => 'text', 'null' => FALSE),
			'var_fieldtype'	=>	array('type' => 'varchar', 'constraint' => '100', 'null' => FALSE)
		);

		$this->EE->load->dbforge();
		$this->EE->dbforge->add_field($fields);
		$this->EE->dbforge->add_key('id', TRUE);
		$this->EE->dbforge->create_table('zoo_plus_settings', TRUE);		
		
		$this->EE->db->insert('actions', array(
			'class'  => 'Zoo_plus_mcp',
			'method' => 'filter_ft'
		));
		
		return TRUE;
	}
	
	function uninstall() 
	{
		// Delete module and his actions
		$this->EE->db->select('module_id');
		$query = $this->EE->db->get_where('modules', array('module_name' => $this->module_name));
		
		$this->EE->db->where('module_id', $query->row('module_id'));
		$this->EE->db->delete('module_member_groups');
		
		$this->EE->db->where('module_name', $this->module_name);
		$this->EE->db->delete('modules');
		
		$this->EE->db->where('class', $this->module_name);
		$this->EE->db->delete('actions');
		
		$this->EE->db->where('class', $this->module_name.'_mcp');
		$this->EE->db->delete('actions');
		
		$this->EE->db->where('site_id', $this->EE->config->item('site_id'));
		$this->EE->db->delete(strtolower($this->module_name).'_settings');
		
		return TRUE;
	}
	
	function update($current = '')
	{

		$this->EE->load->dbforge();
		
		if ($current == '' OR $current == $this->version)
		{
			return FALSE;
		}

		if ($current < 0.3)
		{
			$this->EE->db->insert('actions', array(
				'class'  => 'Zoo_plus_mcp',
				'method' => 'filter_ft'
			));
		}

		$this->EE->db->where('module_name', $this->module_name);
		$this->EE->db->update(
					'modules', 
					array('module_version' => $this->version)
		);
		
	}
	
	
	/**
     * Safe insert of settings, does not overwrite existing settings
     */
	function safe_insert($settings){
		
		foreach($settings as $vars){
		
			$var 			= $vars[0];
			$var_fieldtype 	= $vars[1];
			$var_value 		= $vars[2];
			
			$sql = "SELECT * FROM ".$this->EE->db->dbprefix('zoo_plus_settings')." WHERE site_id = '".$this->EE->config->item('site_id')."' AND var = '".$var."'";
		
			$result = $this->EE->db->query($sql);
		
			if ($result->num_rows() > 0){
	
				//SETTINGS EXISTS, DO NOTHING to AVOID DATA LOSS
							
			}else{
			
				$data = array(
	               'var'	 		=> $var,
	               'var_value' 		=> $var_value,
	               'var_fieldtype' 	=> $var_fieldtype,
	               'site_id' 		=> $this->EE->config->item('site_id')
	            );
	
				$this->EE->db->insert('zoo_plus_settings', $data);
				
			}
		}
	}
	
	
}