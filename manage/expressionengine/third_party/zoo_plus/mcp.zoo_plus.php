<?php 

if (!defined('BASEPATH')) exit('Invalid file request');
require_once PATH_THIRD.'zoo_plus/config.php';

/**
 * Zoo Plus Control Panel Class
 *
 * @package   Zoo Visitor
 * @author    ExpressionEngine Zoo <info@eezoo.com>
 * @copyright Copyright (c) 2011 ExpressionEngine Zoo (http://eezoo.com)
 */
class Zoo_plus_mcp
{
	var $module_name = ZOO_PLUS_NAME;
	var $class_name = ZOO_PLUS_CLASS;
	var $settings = null;
	
	/**
	 * Control Panel Constructor
	 */
	function Zoo_plus_mcp()
	{
		// Make a local reference to the ExpressionEngine super object
		$this->EE =& get_instance();
				
	}
	
	// --------------------------------------------------------------------
	
	function index()
	{
		// Variables
		$vars = array();
		$vars['settings'] = $this->settings;
		
		// Load view
		return $this->_content_wrapper('index', 'index_title', $vars);
	}

	function filter_ft(){

		$plus_relation_direction = ( isset($_POST['plus_relation_direction']) ) ? $_POST['plus_relation_direction'] : '';
		$plus_selected_child_channels = ( isset($_POST['plus_selected_child_channels']) ) ? $_POST['plus_selected_child_channels'] : '';
		$plus_selected_categories = ( isset($_POST['plus_selected_categories']) ) ? $_POST['plus_selected_categories'] : '';
		$plus_field_id = ( isset($_POST['plus_field_id']) ) ? $_POST['plus_field_id'] : '';
		$selected_entries = ( isset($_POST['selected_entries']) ) ? $_POST['selected_entries'] : '';
		$selection_type = ( isset($_POST['selection_type']) ) ? $_POST['selection_type'] : '';
		$keyword = ( isset($_POST['keyword']) ) ? $_POST['keyword'] : '';
		$limit = ( isset($_POST['plus_limit_entries']) ) ? $_POST['plus_limit_entries'] : '';
		
		$selected_entries_pipe = $selected_entries;

		$selected_entries = explode(',',trim($selected_entries, ',') );
		$selected_entry_id = (count($selected_entries) > 0) ? $selected_entries[0] : '';
		
		$entries_options = '';
		$keyword_sql = '';
		$limit_sql = '';
		
		if($keyword != '')
		{
			$keyword_sql = ' AND ct.title LIKE "%'.$keyword.'%"';
		}
		
		if($limit != '')
		{
			$limit_sql = ' LIMIT 0,'.$limit;
		}

		$include_selected = ( $selected_entries_pipe != '' && $keyword == '') ? '( SELECT entry_id, title, entry_date FROM exp_channel_titles WHERE entry_id IN ('.implode(',',$selected_entries).') ) UNION ' : '';

		if($plus_relation_direction == 'to_channel')
		{
			// ==================================
			// = TO CHANNEL get channel entries =
			// ==================================
		
			if( $plus_selected_child_channels != '' && $plus_selected_child_channels != 'false'){
				$channels = ' AND channel_id IN ('.$plus_selected_child_channels.') ';
			}
			else
			{
				$channels = '';
			}
			
			if( $plus_selected_categories != '' && $plus_selected_categories != 'false' )
			{
				
				$sql = $include_selected.' ( SELECT ct.entry_id, ct.title, ct.entry_date from exp_channel_titles ct, exp_category_posts cp WHERE cp.cat_id IN ('.$plus_selected_categories.') AND site_id = "'.$this->EE->config->item('site_id').'"  '.$channels.' '.$keyword_sql.' AND ct.entry_id = cp.entry_id GROUP BY entry_id '.$limit_sql.' ) ORDER BY entry_date DESC ';
			
			}
			else
			{
				$sql = $include_selected.' ( SELECT entry_id, title, entry_date from exp_channel_titles ct WHERE ct.site_id = "'.$this->EE->config->item('site_id').'" '.$channels.' '.$keyword_sql.' '.$limit_sql.' ) ORDER BY entry_date DESC ';
			}
			
			$entries = $this->EE->db->query($sql);
			if($entries->num_rows() > 0 )
			{
				foreach($entries->result() as $row)
				{
					$disabled = '';
					if(in_array($row->entry_id, $selected_entries)){
						$disabled = ($selection_type == 'single') ? '' : ' disabled ';
					}
					$selected = ($selection_type == 'single' && $row->entry_id == $selected_entry_id) ? ' selected ' : ''; 
					$entries_options .= '<option value="'.$row->entry_id.'" '.$selected.' '.$disabled.'>'.$row->title.'</option>';
					
				}
			}
		}
		
		
		if($plus_relation_direction == 'reverse')
		{
			// ====================================
			// = GET ENTRIES FROM REVERSE CHANNEL =
			// ====================================
		
			$sql = $include_selected.' ( SELECT ct.entry_id, ct.title, ct.entry_date FROM exp_channel_titles ct, exp_channel_fields cf, exp_channels ch WHERE cf.field_id = "'.$plus_field_id.'" '.$keyword_sql.' AND cf.group_id = ch.field_group AND ct.channel_id = ch.channel_id '.$limit_sql.') ORDER BY entry_date desc ';
		
			$entries = $this->EE->db->query($sql);
		
			if($entries->num_rows() > 0 )
			{
				foreach($entries->result() as $row)
				{
					$disabled = '';
					if(in_array($row->entry_id, $selected_entries)){
						$disabled = ' disabled ';
					}
					$selected = ($selection_type == 'single' && $row->entry_id == $selected_entry_id) ? ' selected ' : ''; 
					$entries_options .= '<option value="'.$row->entry_id.'" '.$selected.' '.$disabled.'>'.$row->title.'</option>';
					
				}
			}
		}

		$this->send_ajax_response('<option></option>'.$entries_options);
	}
	
	function send_ajax_response($msg, $error = FALSE)
	{
		$this->EE->output->enable_profiler(FALSE);
		
		@header('Content-Type: text/html; charset=UTF-8');	

		exit($msg);
	}
	
	
}