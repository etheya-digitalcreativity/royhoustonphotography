<?php

require_once PATH_THIRD . 'zoo_plus/config.php';

$lang = array(
// -------------------------------------------
//  Module CP
// -------------------------------------------

	'zoo_plus_module_name'          => ZOO_PLUS_NAME,
	'zoo_plus_module_description'   => ZOO_PLUS_DESC,
	'plus_relation_direction'       => 'Relation direction',
	'plus_field_id'                 => 'Related Member profile field',
	'plus_child_channels'           => 'Related Channels',
	'plus_categories'               => 'Restrict to these categories',
	'plus_reverse'                  => 'Reverse relation',
	'plus_to_channel'               => 'Create relation with a channel',
	'select_direction'              => 'Select relation direction',
	'plus_child_channel_empty'      => 'Please select a channel you want to create a relationship with.',
	'plus_relation_direction_empty' => 'Please select a relationship direction',
	'plus_allow_multiple'           => 'Allow multiple selections?',
	'plus_limit_entries'            => 'Limit entries',
	'plus_order_by'                 => 'Order by',
	'plus_sort'                     => 'Sort',
	'plus_editable'                 => 'Editable'
);

?>