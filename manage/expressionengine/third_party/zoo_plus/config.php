<?php

if (!defined('ZOO_PLUS_NAME'))
{
	define('ZOO_PLUS_NAME', 'Zoo Plus');
	define('ZOO_PLUS_CLASS', 'Zoo_plus');
	define('ZOO_PLUS_VER', '0.4');
	define('ZOO_PLUS_DESC', 'Zoo Plus allows you to create visual relationships and reverse relationships');
	define('ZOO_PLUS_DOCS', 'http://eezoo.com/add-ons/plus/');
	define('ZOO_PLUS_UPD', 'http://eezoo.com/add-ons/plus/releasenotes.rss');
	define('ZOO_PLUS_AUTHOR', 'Nico De Gols | EE-Zoo.com');
	
}

// NSM Addon Updater
$config['name'] = ZOO_PLUS_NAME;
$config['version'] = ZOO_PLUS_VER;
$config['nsm_addon_updater']['versions_xml'] = ZOO_PLUS_UPD;