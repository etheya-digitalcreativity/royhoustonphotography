<?php 

if (!defined('BASEPATH')) exit('Invalid file request');
require_once PATH_THIRD.'zoo_plus/config.php';

/**
 * Zoo Plus Class
 *
 * @package   Zoo Plus
 * @author    ExpressionEngine Zoo <hello@ee-zoo.com>
 * @copyright Copyright (c) 2011 ExpressionEngine Zoo (http://ee-zoo.com)
 */
class Zoo_plus
{
	 
	var $version = ZOO_PLUS_VER; 
	var $module_name = ZOO_PLUS_CLASS;
	var $class_name = ZOO_PLUS_CLASS;
	
	var $return_data;
	var $in_forum			= FALSE;
	
	/**
	 * Module Constructor
	 */
	function Zoo_plus()
	{
		// Make a local reference to the ExpressionEngine super object
		$this->EE =& get_instance();
		
		$this->EE->load->add_package_path(PATH_THIRD .'zoo_plus/');
		$this->EE->load->library('zoo_plus_lib');
		$this->EE->load->helper('zoo_plus');
		$this->settings = get_zoo_settings($this->EE);
		
	}

	// ==========================================
	// = Get member related to a provided entry =
	// ==========================================
	function related_members()
	{
		// // Get attributes
		// $field_name = $this->EE->TMPL->fetch_param('field_name', '');
		// $entry_id = $this->EE->TMPL->fetch_param('related_entry_id', '');
		// $tagdata = $this->EE->TMPL->tagdata;
		// 
		// if($field_name != '')
		// {
		// 	$entries_query_arr = array();
		// 	
		// 	$selected_entries_query = $this->EE->db->query('SELECT rel.parent_id as entry_id from exp_zoo_plus_relationships rel, exp_channel_titles ct, exp_channel_fields cf WHERE child_id = "'.$entry_id.'" AND parent_field_id = cf.field_id AND rel.parent_id = ct.entry_id AND cf.field_name = "'.$field_name.'" ORDER BY relation_id DESC');
		// 	
		// 	if($selected_entries_query->num_rows() > 0 )
		// 	{
		// 		$entries_query_arr = $selected_entries_query->result_array();
		// 	}
		// 	
		// 	$entries = get_entries_pipe($entries_query_arr);
		// 	
		// 	return $this->EE->zoo_plus_lib->parse_entries($entries);
		// }
		// else
		// {
		// 	return $this->EE->TMPL->no_results();
		// }

	}
		
	function nested_entries()
	{
		return $this->entries();
	}
	
	function reverse_entries(){
		
		return $this->entries("reverse", FALSE);
		
	}
	function entries($forced_direction = "normal", $using_reverse_field = TRUE )
	{
		$selected_entries = "0";
		
		// Get attributes
		$field_name = $this->EE->TMPL->fetch_param('field', '');
		$parent_id = $this->EE->TMPL->fetch_param('parent_id', '');
		$child_id = $this->EE->TMPL->fetch_param('child_id', '');
		$orderby = $this->EE->TMPL->fetch_param('orderby', '');
		$url_title = $this->EE->TMPL->fetch_param('url_title', '');
		$prefix = $this->EE->TMPL->fetch_param('prefix', 'plus_prefix');
		$exclude = $this->EE->TMPL->fetch_param('exclude', '');
		
		$show_top = $this->EE->TMPL->fetch_param('show_top', false);
		
		$debug = $this->EE->TMPL->fetch_param('debug', 'no');
		
		$tagdata = $this->EE->TMPL->tagdata;

		// ================
		// = IF DEBUG ON  =
		// ================
		$debug_msg = array();
		$debug_msg[] = 'parent_id: '.$parent_id;
		$debug_msg[] = 'field: '.$field_name;
		$debug_msg[] = 'url_title: '.$url_title;
		$debug_msg[] = 'exclude: '.$exclude;
		zoo_debug($this->EE, $debug_msg);

		// =============
		// = Parent_id =
		// =============
		
		if($parent_id == '' && $url_title == '' && $child_id == '')
		{
			echo 'No parent_id or url_title specified';
			return FALSE;
		}
		
		if($parent_id == '' && $child_id == '')
		{
			unset($this->EE->TMPL->tagparams['url_title']);
			$this->EE->db->select('entry_id');
			$this->EE->db->where('url_title', $url_title);
			$parent_id_sql = $this->EE->db->get('channel_titles');
			
			if($parent_id_sql->num_rows() > 0){
				
				$parent_id = $parent_id_sql->row()->entry_id;
			}
			else
			{
				return $this->EE->TMPL->no_results();
			}
		}
		
		// ================================
		// = Get field relation direction =
		// ================================
		
		if($field_name != '')
		{
			
			$field_settings = get_plus_settings($this->EE, $field_name);

			if($field_settings)
			{

				$direction = ($forced_direction == "reverse") ? "reverse" : $field_settings['plus_relation_direction'];
				$field_settings['plus_field_id'] =  ($forced_direction == "reverse")  ? $field_settings['field_id'] : $field_settings['plus_field_id'];
				$parent_id = ($child_id != '') ? $child_id : $parent_id;

				// ================
				// = IF DEBUG ON  =
				// ================
				$debug_msg = array();
				$debug_msg[] = 'direction: '.$direction;
				zoo_debug($this->EE, $debug_msg);

				if($direction == "to_channel")
				{
					
					// $select_count = ", COUNT(rel.child_id) as total_relations";
					// $filter_having = "GROUP BY rel.child_id HAVING COUNT(*) > 0";
					// 
					// // Check if filter must be applied
					// if($member_entry_id == "all")
					// {
					// 	$filter_member_entry_id = "";
					// }
					// else
					// {
					// 	$filter_member_entry_id = 'AND parent_id = "'.$member_entry_id.'"';
					// }
					
					$sql = 'SELECT ct.title, rel.child_id as entry_id from exp_zoo_plus_relationships rel, exp_channel_titles ct WHERE parent_id IN ("'.str_replace('|', '","', $parent_id).'") AND parent_field_id = "'.$field_settings['field_id'].'" AND rel.child_id = ct.entry_id ORDER BY rel_order ASC, relation_id DESC';

				}
				
				if($direction == "reverse")
				{
					$sql = 'SELECT ct.title, rel.parent_id as entry_id from exp_zoo_plus_relationships rel, exp_channel_titles ct WHERE child_id IN ("'.str_replace('|', '","', $parent_id).'")  AND parent_field_id = "'.$field_settings['plus_field_id'].'" AND rel.parent_id = ct.entry_id ORDER BY reverse_rel_order ASC, relation_id DESC';

				}
				
				if($sql != '')
				{
					// ================
					// = IF DEBUG ON  =
					// ================
					$debug_msg = array();
					$debug_msg[] = 'query: '.$sql;
					zoo_debug($this->EE, $debug_msg);
					
					$entries_query_arr =  array();

					$selected_entries_query = $this->EE->db->query($sql);
					
					// $selected_entries_query = $this->EE->db->query('SELECT rel.child_id  as entry_id '.$select_count.' from exp_zoo_plus_relationships rel, exp_channel_titles ct, exp_channel_fields cf WHERE 1 = 1 ' . $filter_member_entry_id . ' AND parent_field_id = cf.field_id AND rel.child_id = ct.entry_id AND cf.field_name = "'.$field_name.'" '.$filter_having.' ORDER BY total_relations DESC, relation_id DESC');
			
					if($selected_entries_query->num_rows() > 0 )
					{
						$entries_query_arr = $selected_entries_query->result_array();
					}	
					else
					{
						$tagdata = $this->EE->zoo_plus_lib->parse_entries('0', false, $prefix);
						return $tagdata;
					}
					
					
					$exclude_arr = explode('|', $exclude);
					
					$entries = get_entries_pipe($entries_query_arr, $exclude_arr);
					
					if($show_top || $orderby == '')
					{
					
						$tagdata = $this->EE->zoo_plus_lib->parse_entries($entries, true, $prefix);
					}
					else
					{
						$tagdata = $this->EE->zoo_plus_lib->parse_entries($entries, false, $prefix);
					}
					
					return $tagdata;
				
				}
				else
				{
					return $this->EE->TMPL->no_results();
				}
			}
			else
			{
				return $this->EE->TMPL->no_results();
			}
		}
	}

	function entries_top()
	{
		$selected_entries = "0";
		
		// Get attributes
		$field_name = $this->EE->TMPL->fetch_param('field', '');
		$limit = $this->EE->TMPL->fetch_param('limit', '');
		$interval = $this->EE->TMPL->fetch_param('interval', '');
		
		$tagdata = $this->EE->TMPL->tagdata;
		
		// ================================
		// = Get field relation direction =
		// ================================
		
		if($field_name != '')
		{
			
			$limitquery = ($limit != '') ? ' LIMIT 0,'.$limit : '';
			$field_settings = get_plus_settings($this->EE, $field_name);
			
			if($field_settings)
			{
				$direction = $field_settings['plus_relation_direction'];
			
				if($direction == "to_channel")
				{
					
					$sql = 'SELECT ct.title, rel.child_id as entry_id, count(rel.child_id) as entries_count from exp_zoo_plus_relationships rel, exp_channel_titles ct WHERE parent_field_id = "'.$field_settings['field_id'].'" AND rel.child_id = ct.entry_id GROUP BY rel.child_id ORDER BY entries_count DESC '.$limitquery;
				
				
				}
				
				if($direction == "reverse")
				{
					$sql = 'SELECT ct.title, rel.parent_id as entry_id, count(rel.parent_id) as entries_count  from exp_zoo_plus_relationships rel, exp_channel_titles ct WHERE parent_field_id = "'.$field_settings['plus_field_id'].'" AND rel.parent_id = ct.entry_id GROUP BY rel.parent_id ORDER BY entries_count DESC '.$limitquery;
					
				}
				
				if($sql != '')
				{
					$entries_query_arr =  array();
			
					$selected_entries_query = $this->EE->db->query($sql);
					
					if($selected_entries_query->num_rows() > 0 )
					{
						$entries_query_arr = $selected_entries_query->result_array();
					}
			
					//get 
					$tag_occurence_entries_count = substr_count($tagdata, '{entries_count}');
					
					$entries = get_entries_pipe($entries_query_arr);
					
					$tagdata = $this->EE->zoo_plus_lib->parse_entries($entries, TRUE);
					
					if(count($entries_query_arr) > 0)
					{
						foreach($entries_query_arr as $entry)
						{
							$tagdata = preg_replace('/{entries_count}/', $entry['entries_count'], $tagdata, $tag_occurence_entries_count);
						}
					}
					else
					{
						$tagdata = str_replace('{entries_count}', '');
					}
					
					return $tagdata;
				
				}
				else
				{
					return $this->EE->TMPL->no_results();
				}
			}
			else
			{
				return $this->EE->TMPL->no_results();
			}
		}
	}
	
	// ====================================================================================================================
	// = check member profile to see if an entry with id 'child_id' is linked through a plus field with name 'field_name' =
	// ====================================================================================================================
	function check_relation()
	{
		// Get attributes
		$field_name = $this->EE->TMPL->fetch_param('field', '');
		$parent_id = $this->EE->TMPL->fetch_param('parent_id', '');
		$child_id = $this->EE->TMPL->fetch_param('child_id', '');
		
		$tagdata = $this->EE->TMPL->tagdata;
		
		$conditional = array();
		$vars = array();
		
		if($field_name == '' || $child_id == '' || $parent_id == ''){
			$vars[] = array('relation_exists_var' => "no");
			$tagdata = $this->EE->TMPL->parse_variables($tagdata, $vars);
			$tagdata = $this->EE->functions->prep_conditionals( $tagdata, array('relation_exists' => FALSE) );
			return $tagdata;
		}
		
		if($this->EE->zoo_plus_lib->check_relation($field_name, $parent_id, $child_id)){
			$conditional = array('relation_exists' => TRUE);
			$vars[] = array('relation_exists_var' => "yes");
		}else{
			$conditional = array('relation_exists' => FALSE);
			$vars[] = array('relation_exists_var' => "no");
		}
		
		$tagdata = $this->EE->TMPL->parse_variables($tagdata, $vars);
		
		$tagdata = $this->EE->functions->prep_conditionals( $tagdata, $conditional );
		return $tagdata;
		
	}
	
	function js_relation_head()
	{
		
		if(isset($_POST['child_id']) && isset($_POST['parent_id']) && isset($_POST['field_name']) && isset($_POST['action'])){
			if($_POST['action'] == 'add'){
				$this->EE->zoo_plus_lib->add_relation($_POST['field_name'], $_POST['parent_id'], $_POST['child_id']);
				exit;
			}
			if($_POST['action'] == 'remove'){
				$this->EE->zoo_plus_lib->remove_relation($_POST['field_name'], $_POST['parent_id'], $_POST['child_id']);
				exit;
			}
			
		}
		
	}
	function js_relation(){
		
		$field_name = $this->EE->TMPL->fetch_param('field', '');
		$parent_id = $this->EE->TMPL->fetch_param('parent_id', '');
		$child_id = $this->EE->TMPL->fetch_param('child_id', '');
		
		if(isset($_POST['child_id']) && isset($_POST['parent_id']) && isset($_POST['field_name']) && isset($_POST['action'])){
			if($_POST['action'] == 'add'){
				$this->EE->zoo_plus_lib->add_relation($_POST['field_name'], $_POST['parent_id'], $_POST['child_id']);
				exit;
			}
			if($_POST['action'] == 'remove'){
				$this->EE->zoo_plus_lib->remove_relation($_POST['field_name'], $_POST['parent_id'], $_POST['child_id']);
				exit;
			}
			
		}else{
				
			$add_txt = clean_ajax_string($this->EE->TMPL->fetch_param('add_txt', 'Add'));
			$remove_txt = clean_ajax_string($this->EE->TMPL->fetch_param('remove_txt', 'Remove'));
			$added_txt = clean_ajax_string($this->EE->TMPL->fetch_param('added_txt', 'Added'));
			$removed_txt = clean_ajax_string($this->EE->TMPL->fetch_param('removed_txt', 'Removed'));
			$processing_txt = clean_ajax_string($this->EE->TMPL->fetch_param('processing_txt', 'loading...'));
			$include_jquery = $this->EE->TMPL->fetch_param('include_jquery', 'no');
			$allow_remove = $this->EE->TMPL->fetch_param('allow_remove', 'no');
			$css_class = $this->EE->TMPL->fetch_param('css_class', '');
			$show_for = $this->EE->TMPL->fetch_param('show_for', 'all');
			
			$domID = $field_name.'_'.$parent_id.'_'.$child_id;
			
			$add_link = '<a href="#" id="zoo_plus_add_rel_'.$domID.'" class="add_relation '.$css_class.'">'.$add_txt.'</a>';
			$remove_link = ($allow_remove == "yes") ? '<a href="#" id="zoo_plus_remove_rel_'.$domID.'" class="remove_relation '.$css_class.'">'.$remove_txt.'</a>' : '';
		
			$ret = '';
			
			if ($include_jquery != 'no') {
				$ret .= '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.0/jquery.min.js"></script>';
			}
		
			//in return check if total is on the page
		
			$ret .= '<script type="text/javascript" charset="UTF-8">';
			
			$ret .= '
						load'.$domID.' = function (){ 
					';
		
			$addJS = '				
							$("#zoo_plus_add_rel_'.$domID.'").live("click", function(event){
								$(".zoo_plus_rel_'.$domID.'").html("'.$processing_txt.'");
								$.ajax({
		  								type: "POST",
										url: "'.$this->EE->input->server("REQUEST_URI").'",
										data: "parent_id='.$parent_id.'&child_id='.$child_id.'&field_name='.$field_name.'&action=add",
										success: function(msg){
											$(".zoo_plus_rel_'.$domID.'").html(\''.$added_txt.' '.$remove_link.'\');
											$("#total_rel_'.$field_name.$child_id.'").html(parseInt($("#total_rel_'.$field_name.$child_id.'").html())+1);
										}
								});
								return false;
							});';

			$removeJS = '
							$("#zoo_plus_remove_rel_'.$domID.'").live("click", function(event){
								$(".zoo_plus_rel_'.$domID.'").html("'.$processing_txt.'");
								$.ajax({
		  								type: "POST",
										url: "'.$this->EE->input->server("REQUEST_URI").'",
										data: "parent_id='.$parent_id.'&child_id='.$child_id.'&field_name='.$field_name.'&action=remove",
										success: function(msg){
											$(".zoo_plus_rel_'.$domID.'").html(\''.$removed_txt.' '.$add_link.'\');
											$("#total_rel_'.$field_name.$child_id.'").html(parseInt($("#total_rel_'.$field_name.$child_id.'").html())-1);
										}
								});
								return false;
							})';
		
			$ret .= $addJS.$removeJS;
		
			$ret .= '
				};
				
				fnc = load'.$domID.';
				if ( typeof window.addEventListener != "undefined" )
				    window.addEventListener( "load", fnc, false );
				  else if ( typeof window.attachEvent != "undefined" ) {
				    window.attachEvent( "onload", fnc );
				  }
				  else {
				    if ( window.onload != null ) {
				      var oldOnload = window.onload;
				      window.onload = function ( e ) {
				        oldOnload( e );
				        window[fnc]();
				      };
				    }
				    else 
				      window.onload = fnc;
				  }

			</script>';
			
			$relation_exists = $this->EE->zoo_plus_lib->check_relation($field_name, $parent_id, $child_id);
			
			if($relation_exists){
				if($allow_remove == "yes"){
					$ret .= $remove_link;
				}
			}else{
				$ret .= $add_link;
			}
			
			$ret = '<span class="zoo_plus_rel_'.$domID.'" class="'.$css_class.'">'.$ret.'</span>';
			
			
			if($show_for == 'only_related' && $relation_exists)
			{
				return $ret;
			}
			if($show_for == 'only_non_related' && !$relation_exists)
			{
				return $ret;
			}	
			if($show_for == 'all')
			{
				return $ret;
			}

		}		
	}
	
	// ================================================
	// = Get total times the entry has been favorited =
	// ================================================
	function total_relations(){
		// Get attributes
		$field_name = $this->EE->TMPL->fetch_param('field', '');
		$child_id = $this->EE->TMPL->fetch_param('child_id', '');		
		$css_class = $this->EE->TMPL->fetch_param('css_class', 'total_relations');
		
		if($field_name == '' || $child_id == ''){
			return 'please provide correct field_name/entry_id parameters';
		}
		
		$vars = array();
		$total_relations = $this->EE->zoo_plus_lib->total_relation($field_name, $child_id);
		$has_relations = ($total_relations > 0) ? TRUE : FALSE;
		$vars[] = array('has_relations' => $has_relations,
						'total_relations_count' => $total_relations,
						'total_relations' => '<span id="total_rel_'.$field_name.$child_id.'" class="'.$css_class.'" >'.$total_relations.'</span>');
	
		return $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, $vars);
	}
	// =================================================================================================
	// = Add an entry with id 'child_id' to the member profile in a plus field with name 'field_name' =
	// =================================================================================================	
	function add_relation()
	{
		
		$tagdata = $this->EE->TMPL->tagdata;
		
		// Get attributes
		$field_name = $this->EE->TMPL->fetch_param('field', '');
		$parent_id = $this->EE->TMPL->fetch_param('parent_id', '');
		$child_id = $this->EE->TMPL->fetch_param('child_id', '');

		$vars = array();
		
		if(!$this->EE->zoo_plus_lib->check_relation($field_name, $parent_id, $child_id)){
			
			if($field_name == '' || $child_id == '' || $parent_id == ''){
				return 'please provide correct field_name/entry_id parameters';
			} 
			
			if( $this->EE->zoo_plus_lib->add_relation($field_name, $parent_id, $child_id) )
			{
				$tagdata = $this->EE->functions->prep_conditionals( $tagdata, array( 'failed' => FALSE, 'success' => TRUE ) );
				$vars[] = array('relation_status' => 'relation_added');
			}
			else
			{
				$tagdata = $this->EE->functions->prep_conditionals( $tagdata, array( 'failed' => TRUE, 'success' => FALSE ) );
				$vars[] = array('relation_status' => 'relation_failed');
			}
			
		}else{
			$tagdata = $this->EE->functions->prep_conditionals( $tagdata, array( 'failed' => TRUE, 'success' => FALSE ) );
			$vars[] = array('relation_status' => 'relation_already_added');
		}
		
		return $this->EE->TMPL->parse_variables($tagdata, $vars);
	}
	
	// =========================================================
	// = Removes the entry from the plus field =
	// =========================================================	
	function remove_relation()
	{
		$tagdata = $this->EE->TMPL->tagdata;
		
		// Get attributes
		$field_name = $this->EE->TMPL->fetch_param('field', '');
		$parent_id = $this->EE->TMPL->fetch_param('parent_id', '');
		$child_id = $this->EE->TMPL->fetch_param('child_id', '');
			
		if($this->EE->zoo_plus_lib->check_relation($field_name, $parent_id, $child_id) )
		{	
			if($field_name == '' || $child_id == '' || $parent_id == ''){
				return 'please provide correct field_name/entry_id parameters';
			} 
			
			if( $this->EE->zoo_plus_lib->remove_relation($field_name, $parent_id, $child_id) )
			{
				$tagdata = $this->EE->functions->prep_conditionals( $tagdata, array( 'failed' => FALSE, 'success' => TRUE ) );
				$vars[] = array('relation_status' => 'relation_removed');
			}
			else
			{
				$tagdata = $this->EE->functions->prep_conditionals( $tagdata, array( 'failed' => TRUE, 'success' => FALSE ) );
				$vars[] = array('relation_status' => 'relation_remove_failed');
			}
		}
		else{
			$tagdata = $this->EE->functions->prep_conditionals( $tagdata, array( 'failed' => TRUE, 'success' => FALSE ) );
			$vars[] = array('relation_status' => 'relation_not_found');
		}
		
		return $this->EE->TMPL->parse_variables($tagdata, $vars);
		
	}
	// =====================================================================
	// = Gets entry ids of the current logged in member OR specific member 
	// = OR piped ids of all members of specific member group =
	// =====================================================================
	function id(){
		$member_id = ($this->EE->TMPL->fetch_param('member_id') != '') ? $this->EE->TMPL->fetch_param('member_id') : 'current';
		$member_group = ($this->EE->TMPL->fetch_param('member_group') != '') ? $this->EE->TMPL->fetch_param('member_group') : '';
		
		if($member_group != ''){
			
			$ids = implode("','", explode('|', trim($member_group)));
			$query_str = "SELECT tit.entry_id FROM exp_members mem, exp_channel_titles tit WHERE tit.author_id = mem.member_id AND tit.channel_id = '".$this->settings['member_channel_id']."' AND mem.group_id IN ('".$ids."') GROUP BY mem.member_id";
			$query = $this->EE->db->query($query_str);
			
			if($query->num_rows() > 0){
				$entry_ids = '0|';
				foreach($query->result() as $row){
					$entry_ids .= $row->entry_id.'|';
				}
				
				return $entry_ids;
			}else{
				return "0|";
			}
				
		}else{ 
			return $this->EE->zoo_plus_lib->get_visitor_id($member_id);
		}
	}
	
}