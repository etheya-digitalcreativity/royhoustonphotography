<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once PATH_THIRD . 'zoo_plus/config.php';

class Zoo_plus_ft extends EE_Fieldtype
{

	var $info = array(
		'name'        => ZOO_PLUS_NAME,
		'version'     => ZOO_PLUS_VER
	);
	var $has_array_data = TRUE;

	/**
	 * Constructor
	 *
	 * @access    public
	 */

	function Zoo_plus_ft()
	{
		parent::__construct();
		// parent::EE_Fieldtype();
		// Brian - Depreciated funtion


		$this->EE->load->add_package_path(PATH_THIRD . 'zoo_plus/');
		$this->EE->load->library('zoo_plus_lib');
		$this->EE->load->helper('zoo_plus');

		//$this->zoo_settings = get_zoo_settings($this->EE);

		$this->EE->lang->loadfile('zoo_plus');

	}

	function replace_tag($data, $params = array(), $tagdata = FALSE)
	{

		return 'AAAA' . $data;
		//$this->EE->zoo_plus_lib->parse_entries($data);
		//echo $data;
		//	return $this->EE->zoo_plus_lib->parse_entries($data);

	}

	function display_field($settings)
	{

		$this->EE->cp->add_js_script(array(
				'ui'        => array('sortable', 'draggable', 'droppable', 'autocomplete')
			)
		);
		$theme_url = rtrim($this->theme_url(), '/') . '/';
		$this->EE->cp->add_to_foot('<script type="text/javascript" src="' . $theme_url . 'js/zoo_plus.js"></script>');
		$this->EE->cp->add_to_foot('<script type="text/javascript" src="' . $theme_url . 'js/chosen.jquery.js"></script>');
		$this->EE->cp->add_to_head('<link rel="stylesheet" type="text/css" href="' . $theme_url . 'css/zoo_plus.css" />');
		$this->EE->cp->add_to_head('<link rel="stylesheet" type="text/css" href="' . $theme_url . 'css/chosen.css" />');

		$selected_entries_li = '';

		$entry_id = $this->EE->input->get('entry_id');

		$entries_options = '';

		$selected_entries_js_array = 'entries = new Array();';

		$selected_entries = array();

		$selected_entry_id = '';

		// ===========================
		// = FIELD IN CHANNEL =
		// ===========================
		if ($this->settings['plus_relation_direction'] == 'to_channel') {
			// =======================================
			// = get selection type: multiple/single =
			// =======================================
			$selection_type = (isset($this->settings['plus_allow_multiple']) && $this->settings['plus_allow_multiple'] == 'yes') ? 'multiple' : 'single';

			//if($entry_id && $entry_id != 0)
			//{
			if (isset($this->cached_entries[$this->settings['field_id']]['selected']) && count($this->cached_entries[$this->settings['field_id']]['selected']) > 0) {
				$where_in_entries       = implode(',', $this->cached_entries[$this->settings['field_id']]['selected']);
				$selected_entries_query = $this->EE->db->query('SELECT entry_id, title from exp_channel_titles WHERE entry_id IN (' . $where_in_entries . ') ORDER BY FIELD (entry_id, ' . $where_in_entries . ')');
			}
			else {
				$selected_entries_query = $this->EE->db->query('SELECT ct.title, rel.child_id as entry_id from exp_zoo_plus_relationships rel, exp_channel_titles ct WHERE parent_id = "' . $entry_id . '" AND parent_field_id = "' . $this->settings['field_id'] . '" AND rel.child_id = ct.entry_id ORDER BY rel_order ASC, relation_id DESC');

			}

			if ($selected_entries_query->num_rows() > 0) {
				$selected_entries = $selected_entries_query->result_array();
			}

			//}

			$selected_entries = array_reverse($selected_entries);

			$c = 0;
			foreach ($selected_entries as $row) {
				if ($c == 0) $selected_entry_id = $row['entry_id'];
				$selected_entries_js_array .= 'entries.push(new Array(' . $this->settings['field_id'] . ',' . $row['entry_id'] . ',"' . htmlentities($row['title'], ENT_QUOTES, "UTF-8") . '"));';
				$c++;
			}

			// =======================
			// = get channel entries =
			// =======================
			// if(count($this->settings['plus_selected_child_channels']) > 0 && $this->settings['plus_selected_child_channels'] != ''){
			// 	$channels = ' AND channel_id IN ('.$this->settings['plus_selected_child_channels'].') ';
			// }
			// else
			// {
			// 	$channels = '';
			// }
			//
			// if( isset( $this->settings['plus_selected_categories'] ) && $this->settings['plus_selected_categories'] != '' )
			// {
			//
			// 	$sql = 'SELECT ct.entry_id, ct.title from exp_channel_titles ct, exp_category_posts cp WHERE cp.cat_id IN ('.$this->settings['plus_selected_categories'].') AND site_id = "'.$this->EE->config->item('site_id').'"  '.$channels.' AND ct.entry_id = cp.entry_id GROUP BY ct.entry_id ORDER BY title ASC';
			//
			// }
			// else
			// {
			// 	$sql = 'SELECT entry_id, title from exp_channel_titles WHERE site_id = "'.$this->EE->config->item('site_id').'" '.$channels.'  ORDER BY title ASC';
			// }
			//
			//
			// $entries = $this->EE->db->query($sql);
			// if($entries->num_rows() > 0 )
			// {
			// 	foreach($entries->result() as $row)
			// 	{
			// 		$selected = ($selection_type == 'single' && $row->entry_id == $selected_entry_id) ? ' selected ' : '';
			// 		$entries_options .= '<option value="'.$row->entry_id.'" '.$selected.' >'.$row->title.'</option>';
			// 	}
			// }

		}

		// =========================================
		// = SHOW REVERSE ENTRIES				   =
		// =========================================
		if ($this->settings['plus_relation_direction'] == 'reverse') {

			// ==============================================================
			// = get selection type based on reverse field: multiple/single =
			// ==============================================================
			$reverse_settings = get_plus_settings($this->EE, '', $this->settings['plus_field_id']);
			$selection_type   = 'multiple'; //( isset($reverse_settings['plus_allow_multiple']) && $reverse_settings['plus_allow_multiple'] == 'yes' ) ? 'multiple' : 'single';

			//if($entry_id && $entry_id != 0)
			//{
			if (isset($this->cached_entries[$this->settings['field_id']]['selected']) && count($this->cached_entries[$this->settings['field_id']]['selected']) > 0) {
				$where_in_entries       = implode(',', $this->cached_entries[$this->settings['field_id']]['selected']);
				$selected_entries_query = $this->EE->db->query('SELECT entry_id, title from exp_channel_titles WHERE entry_id IN (' . $where_in_entries . ') ORDER BY FIELD (entry_id, ' . $where_in_entries . ')');
			}
			else {
				$selected_entries_query = $this->EE->db->query('SELECT ct.title, rel.parent_id as entry_id from exp_zoo_plus_relationships rel, exp_channel_titles ct WHERE child_id = "' . $entry_id . '" AND parent_field_id = "' . $this->settings['plus_field_id'] . '" AND rel.parent_id = ct.entry_id ORDER BY reverse_rel_order ASC, relation_id DESC');
			}

			if ($selected_entries_query->num_rows() > 0) {
				$selected_entries = $selected_entries_query->result_array();
			}
			//}

			$selected_entries = array_reverse($selected_entries);

			$c = 0;
			foreach ($selected_entries as $row) {
				if ($c == 0) $selected_entry_id = $row['entry_id'];
				$selected_entries_js_array .= 'entries.push(new Array(' . $this->settings['field_id'] . ',' . $row['entry_id'] . ',"' . htmlentities($row['title'], ENT_QUOTES, "UTF-8") . '"));';
				$c++;
			}

			// ====================================
			// = GET ENTRIES FROM REVERSE CHANNEL =
			// ====================================

			// $sql = 'SELECT ct.entry_id, ct.title FROM exp_channel_titles ct, exp_channel_fields cf, exp_channels ch WHERE cf.field_id = "'.$this->settings['plus_field_id'].'" AND cf.group_id = ch.field_group AND ct.channel_id = ch.channel_id ORDER BY ct.entry_date desc';
			//
			// 			$entries = $this->EE->db->query($sql);
			//
			// 			if($entries->num_rows() > 0 )
			// 			{
			// 				foreach($entries->result() as $row)
			// 				{
			// 					$selected = ($selection_type == 'single' && $row->entry_id == $selected_entry_id) ? ' selected ' : '';
			// 					$entries_options .= '<option value="'.$row->entry_id.'" '.$selected.' >'.$row->title.'</option>';
			// 				}
			// 			}

		}

		$entries_options = '';

		$display_selected_entries_ul = ($selection_type == 'multiple') ? '' : ' style="display:none;" ';

		$entries_select = (isset($this->settings['plus_editable']) && $this->settings['plus_editable'] != 'yes') ? '' : '<select data-placeholder="- Select an entry -" id="channel_entries' . $this->settings['field_id'] . '" name="channel_entries' . $this->settings['field_id'] . '" class="entries_select"><option value="">- Select an entry -</option>' . $entries_options . '</select>';

		$selected_entries_ul = '<ul id="selected_entries_ul' . $this->settings['field_id'] . '" class="selected_entries" ' . $display_selected_entries_ul . ' ></ul>';

		$existing_entries_pipe = (isset($_POST['old_selected_entries' . $this->settings['field_id']])) ? $_POST['old_selected_entries' . $this->settings['field_id']] : $settings;
		$old_selected_entries  = '<input type="hidden" name="old_selected_entries' . $this->settings['field_id'] . '" value="' . $existing_entries_pipe . '"/>';


		$query     = $this->EE->db->query('SELECT action_id FROM exp_actions WHERE class = "Zoo_plus_mcp" AND method = "filter_ft"');
		$action_id = $query->num_rows() ? $query->row('action_id') : FALSE;

		if (($site_index = $this->EE->config->item('zoo_plus_site_index')) === FALSE) $site_index = $this->EE->functions->fetch_site_index(0, 0);

		$filter_url = $site_index . QUERY_MARKER . 'ACT=' . $action_id;

		$this->settings['filter_url']     = $filter_url;
		$this->settings['selection_type'] = $selection_type;
		$this->settings['width'] = '95%'; //Plus width percentage


		$json_settings = (json_encode ($this->settings, TRUE));

		// $json_settings = $this->EE->javascript->json_encode($this->settings, TRUE);
		// Brian - change
		// Deprecated function generate_json()
		// Deprecated since 2.6. Use the native JSON extension (json_encode()) instead.



		$js = '<script>

				function loadZooPlus' . $this->settings['field_id'] . '(){
					field_id = ' . $this->settings['field_id'] . ';
					' . $selected_entries_js_array . '

					$.each(entries, function(index, value) {
						field_id = value[0];
						entry_id = value[1];
						text	 = value[2];

						addToList(field_id, entry_id, text, "' . $selection_type . '", ' . $json_settings . ');
					});

					initPlusList(field_id, "' . $selection_type . '", ' . $json_settings . ');
				}

				fnc = loadZooPlus' . $this->settings['field_id'] . ';

				if ( typeof window.addEventListener != "undefined" )
				    window.addEventListener( "load", fnc, false );
				  else if ( typeof window.attachEvent != "undefined" ) {
				    window.attachEvent( "onload", fnc );
				  }
				  else {
				    if ( window.onload != null ) {
				      var oldOnload = window.onload;
				      window.onload = function ( e ) {
				        oldOnload( e );
				        window[fnc]();
				      };
				    }
				    else
				      window.onload = fnc;
				  }
		</script>';

		return $js . $entries_select . $selected_entries_ul . $old_selected_entries;
	}

	function theme_url()
	{
		$theme_folder_url = $this->EE->config->item('theme_folder_url');
		if (substr($theme_folder_url, -1) != '/') $theme_folder_url .= '/';
		return $theme_folder_url . 'third_party/zoo_plus/';
	}

	function validate($data)
	{
		$this->cached_entries[$this->settings['field_id']] = $data;
	}

	function save($data)
	{
		$this->cached_entries[$this->settings['field_id']] = $data;
		return (isset($data['selected']) && count($data['selected']) > 0) ? implode('|', array_filter($data['selected'])) : '';
	}

	function post_save($data)
	{

		$existing_entries = (isset($_POST['old_selected_entries' . $this->settings['field_id']])) ? explode('|', $_POST['old_selected_entries' . $this->settings['field_id']]) : array();

		if ($this->settings['plus_relation_direction'] == 'to_channel') {
			if (isset($this->cached_entries[$this->settings['field_id']])) {
				if (isset($this->cached_entries[$this->settings['field_id']]['selected'])) {

					// ========================================================
					// = only delete entries that are not in the new selected =
					// ========================================================
					$selected_entries = $this->cached_entries[$this->settings['field_id']]['selected'];

					$removeArr = array_values(array_diff($existing_entries, $selected_entries));

					foreach ($removeArr as $remove_id) {
						$this->EE->db->where('parent_field_id', $this->settings['field_id'])->where('parent_id', $this->settings['entry_id'])->where('child_id', $remove_id)->delete('zoo_plus_relationships');
					}

					$insert    = array();
					$insertArr = $selected_entries; //array_values(array_diff($selected_entries, $existing_entries));

					// ====================================
					// = only insert new selected entries =
					// ====================================
					$order = 0;
					foreach ($insertArr as $insert_entry_id) {
						$parent_id       = $this->settings['entry_id'];
						$parent_field_id = $this->settings['field_id'];
						$child_id        = $insert_entry_id;
						save_entry($this->EE, $parent_id, $child_id, $parent_field_id, $order, FALSE);
						$order++;
					}

				}
				else {
					//remove all, selected is empty
					$this->EE->db->where('parent_field_id', $this->settings['field_id'])->where('parent_id', $this->settings['entry_id'])->delete('zoo_plus_relationships');

				}
			}
		}

		if ($this->settings['plus_relation_direction'] == 'reverse' && isset($this->settings['plus_field_id']) && $this->settings['plus_field_id'] != '') {
			if (isset($this->cached_entries[$this->settings['field_id']])) {

				if (isset($this->cached_entries[$this->settings['field_id']]['selected'])) {

					// ========================================================
					// = only delete entries that are not in the new selected =
					// ========================================================
					$selected_entries = $this->cached_entries[$this->settings['field_id']]['selected'];

					$removeArr = array_values(array_diff($existing_entries, $selected_entries));

					foreach ($removeArr as $remove_id) {
						$this->EE->db->where('parent_field_id', $this->settings['plus_field_id'])->where('child_id', $this->settings['entry_id'])->where('parent_id', $remove_id)->delete('zoo_plus_relationships');

					}

					$insert    = array();
					$insertArr = $selected_entries; //array_values(array_diff($selected_entries, $existing_entries));

					// ====================================
					// = only insert new selected entries =
					// ====================================
					$order = 0;
					foreach ($insertArr as $insert_entry_id) {
						$parent_id       = $insert_entry_id;
						$parent_field_id = $this->settings['plus_field_id'];
						$child_id        = $this->settings['entry_id'];
						save_entry($this->EE, $parent_id, $child_id, $parent_field_id, $order, TRUE);
						$order++;
					}

				}
				else {
					$this->EE->db->where('parent_field_id', $this->settings['plus_field_id'])->where('child_id', $this->settings['entry_id'])->delete('zoo_plus_relationships');

				}

			}
		}


	}

	public function display_settings($data)
	{

		// =====================
		// = CHANNEL RELATION  =
		// =====================
		//get zoo plus relation fields from the member channel fieldgroup, relation fields that already have been assigned
		$plus_fields = array();
		$query       = $this->EE->db->query("SELECT ch.*, cf.* FROM exp_channels ch, exp_channel_fields cf where ch.site_id = '" . $this->EE->config->item('site_id') . "' AND ch.field_group = cf.group_id AND cf.field_type = 'zoo_plus'");
		foreach ($query->result() as $row) {
			$field_settings = unserialize(base64_decode($row->field_settings));

			if (isset($_GET['field_id']) && $_GET['field_id'] == $row->field_id) {
				//do not include this field
			}
			else {
				if ($field_settings['plus_relation_direction'] == 'to_channel') {
					$plus_fields[$row->field_id] = $row->channel_name . ' &raquo; ' . $row->field_name;
				}
			}
		}

		//if direction is channel_to_member, select which member field we are reversing
		$plus_field_id = isset($data['plus_field_id']) ? $data['plus_field_id'] : '';

		$plus_field_id_field = (count($plus_fields) > 0) ? form_dropdown('plus_field_id', $plus_fields, $plus_field_id, 'id = "plus_field_row" class= "plus_field_row plus_input"') : '<div id="plus_field_row" class="plus_field_row">' . lang('no_plus_fields', 'There are no zoo visitor relation fields found.') . '</div>';

		// =====================
		// = REVERSE RELATION  =
		// =====================
		//Grab channels
		$this->EE->load->model('channel_model');
		$channels_query = $this->EE->channel_model->get_channels();
		$child_channels = array();
		foreach ($channels_query->result_array() as $channel) {
			$child_channels[$channel['channel_id']] = $channel['channel_title'];
		}

		//get set channels
		$selected_child_channels = array();

		if (isset($data['plus_selected_child_channels'])) {
			$selected_child_channels = explode(',', $data['plus_selected_child_channels']);
		}

		//get set categories
		$selected_categories = array();

		if (isset($data['plus_selected_categories'])) {
			$selected_categories = explode(',', $data['plus_selected_categories']);
		}

		// ===================
		// = Category groups =
		// ===================
		$this->EE->load->model('category_model');
		$this->EE->load->library('api');
		$this->EE->api->instantiate('channel_categories');


		$categories = $this->EE->category_model->get_category_groups('', FALSE);

		// $categories = $this->EE->category_model->get_categories('', FALSE);

		// Brian - Deprecated function get_categories()
		// Deprecated since 2.2.0. Use Category_model::get_category_groups() instead.


		$all_categories = array('' => 'All categories');
		foreach ($categories->result() as $row) {
			// Fetch the category tree
			$this->EE->api_channel_categories->category_tree($row->group_id);

			$vars['categories'] = array();

			if (count($this->EE->api_channel_categories->categories) > 0) {
				$all_categories[$row->group_name] = array();
				foreach ($this->EE->api_channel_categories->categories as $key => $val) {
					$all_categories[$row->group_name][$key] = $val[1];
				}
			}
			else {

			}

		}


		//}

		//Which way is the relationship, options are to_channel / reverse
		$relation_direction = isset($data['plus_relation_direction']) ? $data['plus_relation_direction'] : '';

		$relation_direction_options               = array();
		$relation_direction_options['']           = '--- ' . lang('select_direction') . ' ---';
		$relation_direction_options['to_channel'] = lang('plus_to_channel');
		$relation_direction_options['reverse']    = lang('plus_reverse');

		$js = '<script type="text/javascript" charset="UTF-8">';
		$js .= '
				$(document).ready(function() {
					$(".plus_member_field_row").parent().parent().hide();
					$(".plus_child_channels_row").parent().parent().hide();
				';

		$js .= 'changeDirection("' . $relation_direction . '");';

		if (count($plus_fields) == 0) {
			$js .= '$("#plus_relation_direction option[value=\'reverse\']").attr("disabled", "disabled");';
		}
		$js .= '});


				$("#plus_relation_direction").change(function() {
					changeDirection($(this).val());
				});

				function changeDirection(dir)
				{
					if(dir == "to_channel")
					{
						$(".plus_field_row").parent().parent().hide();
						$(".plus_child_channels").parent().parent().show();
						$(".plus_categories").parent().parent().show();
						$(".plus_allow_multiple").parent().parent().show();
						$(".plus_limit_entries").parent().parent().show();
						$(".plus_order_by").parent().parent().show();
						$(".plus_sort").parent().parent().show();
						$(".plus_editable").parent().parent().hide();
					}
					if(dir == "reverse")
					{
						$(".plus_field_row").parent().parent().show();
						$(".plus_child_channels").parent().parent().hide();
						$(".plus_categories").parent().parent().hide();
						$(".plus_allow_multiple").parent().parent().hide();
						$(".plus_limit_entries").parent().parent().show();
						$(".plus_order_by").parent().parent().hide();
						$(".plus_sort").parent().parent().hide();
						$(".plus_editable").parent().parent().show();
					}
					if(dir == "")
					{
						$(".plus_field_row").parent().parent().hide();
						$(".plus_child_channels").parent().parent().hide();
						$(".plus_categories").parent().parent().hide();
						$(".plus_allow_multiple").parent().parent().hide();
						$(".plus_limit_entries").parent().parent().hide();
						$(".plus_order_by").parent().parent().hide();
						$(".plus_sort").parent().parent().hide();
						$(".plus_editable").parent().parent().hide();
					}
				}';

		$js .= '</script>';

		$css = '<style type="text/css">.plus_input{width:50%;}</style>';
		$this->EE->table->add_row(
			lang('plus_relation_direction', 'plus_relation_direction'),
			form_dropdown('plus_relation_direction', $relation_direction_options, $relation_direction, 'id = "plus_relation_direction" class="plus_relation_direction plus_input"') . $js . $css
		);

		$this->EE->table->add_row(
			lang('plus_child_channels', 'plus_child_channels'),
			form_multiselect('plus_selected_child_channels[]', $child_channels, $selected_child_channels, 'id = "plus_child_channels" class = "plus_child_channels plus_input"')
		);

		$this->EE->table->add_row(
			lang('plus_categories', 'plus_categories'),
			form_multiselect('plus_selected_categories[]', $all_categories, $selected_categories, 'id = "plus_categories" class = "plus_categories plus_input"')
		);

		$plus_allow_multiple         = isset($data['plus_allow_multiple']) ? $data['plus_allow_multiple'] : 'yes';
		$plus_allow_multiple_checked = ($plus_allow_multiple == "yes") ? TRUE : FALSE;

		$this->EE->table->add_row(
			lang('plus_allow_multiple', 'plus_allow_multiple'),
			form_checkbox('plus_allow_multiple', 'yes', $plus_allow_multiple_checked, 'id = "plus_allow_multiple" class = "plus_allow_multiple"')
		);

		$plus_limit_entries = isset($data['plus_limit_entries']) ? $data['plus_limit_entries'] : '';

		$this->EE->table->add_row(
			lang('plus_limit_entries', 'plus_limit_entries'),
			form_dropdown('plus_limit_entries', array(''    => 'Show all',
			                                          '25'  => '25',
			                                          '50'  => '50',
			                                          '100' => '100',
			                                          '200' => '200',
			                                          '400' => '400'), $plus_limit_entries, 'id = "plus_limit_entries" class = "plus_limit_entries plus_input"')
		);

		$plus_order_by = isset($data['plus_order_by']) ? $data['plus_order_by'] : '';

		$this->EE->table->add_row(
			lang('plus_order_by', 'plus_order_by'),
			form_dropdown('plus_order_by', array('entry_date' => 'Entry date',
			                                     'title'      => 'Alphabetically by title'), $plus_order_by, 'id = "plus_order_by" class = "plus_order_by plus_input"')
		);

		$plus_sort = isset($data['plus_sort']) ? $data['plus_sort'] : '';

		$this->EE->table->add_row(
			lang('plus_sort', 'plus_sort'),
			form_dropdown('plus_sort', array('desc' => 'Descending',
			                                 'asc'  => 'Ascending'), $plus_sort, 'id = "plus_sort" class = "plus_sort plus_input"')
		);

		$this->EE->table->add_row(
			lang('plus_field_id', 'plus_field_id'),
			$plus_field_id_field
		);

		$plus_editable         = isset($data['plus_editable']) ? $data['plus_editable'] : 'yes';
		$plus_editable_checked = ($plus_editable == "yes") ? TRUE : FALSE;

		$this->EE->table->add_row(
			lang('plus_editable', 'plus_editable'),
			form_checkbox('plus_editable', 'yes', $plus_editable_checked, 'id = "plus_editable" class = "plus_editable"')
		);
	}

	public function save_settings($data)
	{

		if (empty($_POST)) {
			show_error($this->EE->lang->line('unauthorized_access'));
		}

		$selected_child_channels = '';

		$selected_child_channels = $this->EE->input->post('plus_selected_child_channels');
		if (is_array($selected_child_channels)) {
			$selected_child_channels = implode(',', $selected_child_channels);
		}

		$selected_categories = '';

		$selected_categories = $this->EE->input->post('plus_selected_categories');

		if (is_array($selected_categories)) {
			$selected_categories = implode(',', $selected_categories);
		}

		$relation_direction = $this->EE->input->post('plus_relation_direction');

		//only set plus_field_id if the relation is channel_to_member
		$plus_field_id = ($relation_direction == "reverse") ? $this->EE->input->post('plus_field_id') : '';

		//only set child channels if relation is member => channel
		$selected_child_channels = ($relation_direction == "to_channel") ? $selected_child_channels : '';

		$plus_allow_multiple = $this->EE->input->post('plus_allow_multiple');

		$plus_limit_entries = $this->EE->input->post('plus_limit_entries');

		$plus_order_by = $this->EE->input->post('plus_order_by');

		$plus_sort = $this->EE->input->post('plus_sort');

		$plus_editable = $this->EE->input->post('plus_editable');


		$settings = array(
			'plus_selected_child_channels'      => $selected_child_channels,
			'plus_selected_categories'          => $selected_categories,
			'plus_relation_direction'           => $relation_direction,
			'plus_field_id'                     => $plus_field_id,
			'plus_allow_multiple'               => $plus_allow_multiple,
			'plus_limit_entries'                => $plus_limit_entries,
			'plus_order_by'                     => $plus_order_by,
			'plus_sort'                         => $plus_sort,
			'plus_editable'                     => $plus_editable

		);

		return $settings;
	}


	function install()
	{

		// zoo visitor settings
		$fields = array(
			'relation_id'              => array('type'           => 'int',
			                                    'constraint'     => '10',
			                                    'unsigned'       => TRUE,
			                                    'null'           => FALSE,
			                                    'auto_increment' => TRUE),
			'parent_id'                => array('type'       => 'int',
			                                    'constraint' => '8',
			                                    'unsigned'   => TRUE,
			                                    'null'       => FALSE,
			                                    'default'    => '0'),
			'parent_field_id'          => array('type'       => 'int',
			                                    'constraint' => '8',
			                                    'unsigned'   => TRUE,
			                                    'null'       => FALSE,
			                                    'default'    => '0'),
			'child_id'                 => array('type'       => 'int',
			                                    'constraint' => '8',
			                                    'unsigned'   => TRUE,
			                                    'null'       => FALSE,
			                                    'default'    => '0'),
			'child_field_id'           => array('type'       => 'int',
			                                    'constraint' => '8',
			                                    'unsigned'   => TRUE,
			                                    'null'       => FALSE,
			                                    'default'    => '0'),
			'rel_order'                => array('type'       => 'int',
			                                    'constraint' => '8',
			                                    'unsigned'   => TRUE,
			                                    'null'       => FALSE,
			                                    'default'    => '0'),
			'reverse_rel_order'        => array('type'       => 'int',
			                                    'constraint' => '8',
			                                    'unsigned'   => TRUE,
			                                    'null'       => FALSE,
			                                    'default'    => '0'),
			'status'                   => array('type'       => 'varchar',
			                                    'constraint' => '50',
			                                    'default'    => '0'),
			'creationdate'             => array('type' => 'timestamp')

		);

		$this->EE->load->dbforge();
		$this->EE->dbforge->add_field($fields);
		$this->EE->dbforge->add_key('relation_id', TRUE);
		$this->EE->dbforge->create_table('zoo_plus_relationships', TRUE);

	}

}

?>