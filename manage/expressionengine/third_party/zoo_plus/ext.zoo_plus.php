<?php
@session_start();
if (!defined('BASEPATH')) exit('Invalid file request');
require_once PATH_THIRD . 'zoo_plus/config.php';
/**
 * Zoo plus Extension Class
 *
 * @package   Zoo plus
 * @author    ExpressionEngine Zoo <info@eezoo.com>
 * @copyright Copyright (c) 2011 ExpressionEngine Zoo (http://eezoo.com)
 */
class Zoo_plus_ext
{
	var $base;
	var $form_base;
	var $name = ZOO_PLUS_NAME;
	var $class_name = ZOO_PLUS_CLASS;
	var $settings_exist = 'n';
	var $docs_url = ZOO_PLUS_DOCS;
	var $version = ZOO_PLUS_VER;

	var $settings_default = array();
	var $field_errors = array();

	/**
	 * Extension Constructor
	 */
	function Zoo_plus_ext()
	{
		$this->EE =& get_instance();

		$this->EE->load->add_package_path(PATH_THIRD . 'zoo_plus/');
		//$this->EE->load->library('zoo_plus_lib');
		$this->EE->load->helper('zoo_plus');
		$this->settings = get_plus_settings($this->EE, '', '', TRUE);
	}

	function hook_channel_entries_tagdata($tagdata, $row, &$Channel)
	{

		if ($this->EE->extensions->last_call) {
			$tagdata = $this->EE->extensions->last_call;
		}

		foreach ($this->zoo_fields() as $field_id => $field_name) {
			$tagdata = str_replace('{'.$field_name, '{exp:zoo_plus:entries parent_id="{entry_id}" field="'.$field_name.'" ', $tagdata);
			$tagdata = str_replace('{/'.$field_name, '{/exp:zoo_plus:entries', $tagdata);
		}

		//		$tagdata = str_replace('{products_related', '{exp:zoo_plus:entries parent_id="{entry_id}" field="products_related"', $tagdata);
//		$tagdata = str_replace('{/products_related', '{/exp:zoo_plus:entries', $tagdata);

		return $tagdata;

	}

	function zoo_fields()
	{

		$site_id = $this->EE->config->item('site_id');

		if (!isset($this->EE->session->cache['zoo_plus'])) {
			$this->EE->session->cache['zoo_plus'] = array();
		}

		$cache = &$this->EE->session->cache['zoo_plus'];

		if (!isset($cache['site_fields'][$site_id])) {
			$this->EE->db->select('field_id, field_name');
			$this->EE->db->where('field_type', 'zoo_plus');
			if ($site_id) $this->EE->db->where('site_id', $site_id);

			$fields = $this->EE->db->get('channel_fields')->result();

			if ($fields) {
				foreach ($fields as $field)
				{
					$cache['site_fields'][$site_id][$field->field_id] = $field->field_name;
				}
			}
			else
			{
				$cache['site_fields'][$site_id] = array();
			}
		}

		return $cache['site_fields'][$site_id];
	}

	// ==================================
	// = Activate Zoo Visitor Extension =
	// ==================================
	function activate_extension()
	{
		//if activation is set to none,
		//=>insert channel entry if activation is set to none, 
		//=>auto-login the member
		//=>if set, redirect to full profile page 

		$this->EE->db->insert('extensions', array(
			'class'    => 'Zoo_plus_ext',
			'hook'     => 'channel_entries_tagdata',
			'method'   => 'hook_channel_entries_tagdata',
			'settings' => '',
			'priority' => 1,
			'version'  => $this->version,
			'enabled'  => 'y'
		));

	}

	// ================================
	// = Update Zoo Visitor Extension =
	// ================================
	function update_extension($current = FALSE)
	{
		if ($current == '' OR $current == $this->version) {
			return FALSE;
		}

		if ($current < '1.0.0') {

		}

		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->update(
			'extensions',
			array('version' => $this->version)
		);

	}

	// ================================
	// = Disable Zoo Visitor Extension =
	// ================================
	function disable_extension()
	{
		$this->EE->db->query('DELETE FROM exp_extensions WHERE class = "Zoo_plus_ext"');
	}

}
