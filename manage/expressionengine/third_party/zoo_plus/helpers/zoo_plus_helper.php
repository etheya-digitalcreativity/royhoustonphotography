<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('zoo_debug'))
{	
	function zoo_debug($EE, $msg = array()) {
		
		$debug = $EE->TMPL->fetch_param('debug', 'no');
		
		if($debug == 'yes' && count($msg) > 0)
		{
			foreach($msg as $line) $EE->TMPL->log_item( '>>>> Zoo Plus debug: ' . $line );
			
		}
		
	}

}

if ( ! function_exists('save_entry'))
{	
	function save_entry($EE, $parent_id = 0, $child_id = 0, $parent_field_id = 0, $order = 0, $is_reverse = FALSE) {
		
		if ($parent_id != 0 && $child_id != 0 && $parent_field_id != 0) {
		
			$record = array('parent_id'=>$parent_id, 'child_id'=>$child_id, 'parent_field_id'=>$parent_field_id);
			
			// Check if a record exists for this combination
			$EE->db->where($record);
			$query = $EE->db->get('zoo_plus_relationships');
			
			//reverse field?
			if($is_reverse)
			{
				$record['reverse_rel_order'] = $order;
			}
			else
			{	
				$record['rel_order'] = $order;
			}
			
			if ($query->num_rows() == 0) {
				// A record does not exist, insert one.
				$query = $EE->db->insert('zoo_plus_relationships', $record);
				
			} 
			else 
			{
				// A record does exist, update it.
				$query = $EE->db->update('zoo_plus_relationships', $record, array('relation_id'=>$query->row()->relation_id) );
			}
			// Check to see if the query actually performed correctly
			if ($EE->db->affected_rows() > 0) {
				return TRUE;
			}
		}
		return FALSE;
	}

}

if ( ! function_exists('get_plus_settings'))
{
	function get_plus_settings($EE, $field_name = '', $field_id = '')
	{
	
		if($field_name == '' && $field_id == '') return FALSE;
		
		$field_settings = array();
		
		if($field_name != '')
		{
			$sql = 'SELECT field_id, field_settings from exp_channel_fields cf WHERE 1 = 1 AND cf.field_name = "'.$field_name.'"';
		}
		if($field_id != '')
		{
			$sql = 'SELECT field_id, field_settings from exp_channel_fields cf WHERE 1 = 1 AND cf.field_id = "'.$field_id.'"';
		}
		$field_query = $EE->db->query($sql);
		
		if($field_query->num_rows() > 0 )
		{
			$field_settings = unserialize(base64_decode($field_query->row()->field_settings));
			$field_settings['field_id'] = $field_query->row()->field_id;
			return $field_settings;
		}
		else
		{
			return FALSE;
		}
		
	}
	
}

if ( ! function_exists('get_zoo_settings'))
{
	function get_zoo_settings($EE)
	{
	
		$settings = array();
		
		if ($EE->db->table_exists('zoo_visitor_settings')){
		
			$sql = "SELECT * FROM ".$EE->db->dbprefix('zoo_visitor_settings')." WHERE site_id = '".$EE->config->item('site_id')."'";
	
			$result = $EE->db->query($sql);
	
			if ($result->num_rows() > 0)
			{
				foreach ($result->result_array() as $row)
				{
					
					$settings[$row['var']] = $row['var_value'];
					$settings["fields"][$row['var']] = $row['var_fieldtype'];
					
				}
			}
			
			if(isset($settings['member_channel_id'])){
				$sql = "SELECT * FROM ".$EE->db->dbprefix('channels')." WHERE channel_id = '".$settings['member_channel_id']."' AND site_id = '".$EE->config->item('site_id')."'";

				$result = $EE->db->query($sql);

				if ($result->num_rows() > 0)
				{
					$settings['member_channel_name'] = $result->row()->channel_name;
				}
			}
		
		}

		return $settings;
	}
	
}

if ( ! function_exists('format_status'))
{
	function format_status($group_title, $group_id){
		return preg_replace("/[^a-z0-9_]/i",'_', $group_title).'-id'.$group_id;
	}
}

if ( ! function_exists('clean_ajax_string'))
{	
	function clean_ajax_string($str){
		if(strpos($str, 'img')){
			return addslashes($str);
		}else{
			return htmlentities($str, ENT_QUOTES, "UTF-8");
		}
		
	}
}

if ( ! function_exists('get_entries_pipe'))
{	
	function get_entries_pipe($entries_arr = array(), $exclude_arr = array())
	{
		$selected_entries_arr = array();
		
		if(isset($entries_arr) && count($entries_arr) > 0)
		{
			foreach($entries_arr as $key => $val)
			{
				array_push($selected_entries_arr, $val['entry_id']);
			}
		}
		
		$selected_entries_arr = array_diff($selected_entries_arr, $exclude_arr);
			
		return (count($selected_entries_arr) > 0) ? implode('|', $selected_entries_arr) : 0;
	}
}		
?>